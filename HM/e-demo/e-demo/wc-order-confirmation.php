<!DOCTYPE html>
<html data-wf-page="5ebbe78b6291d6110ecb2ee1" data-wf-site="5ea7a66e99f15228d9bd936e"><head>
  <meta charset="utf-8">
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1589866536765" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1589866536765" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/e-demo.webflow.css?v=1589866536765" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Inconsolata:400,700","Pacifico:regular","Caveat:regular","Bree Serif:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1589866536765" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1589866536765" rel="apple-touch-icon">
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('order-confirmation'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="order-confirmation"><?php global $wc_order; ?>
  <div data-node-type="commerce-order-confirmation-wrapper" data-wf-order-query="" data-wf-page-link-href-prefix="" class="w-commerce-commerceorderconfirmationcontainer">
    <div class="w-commerce-commercelayoutcontainer w-container">
      <div class="w-commerce-commercelayoutmain">
        <div class="w-commerce-commercecheckoutcustomerinfosummarywrapper">
          <div class="w-commerce-commercecheckoutsummaryblockheader">
            <h4 data-udy-fe="text_-54cb6016"><?php echo $udesly_fe_items['text_-54cb6016'] ?></h4>
          </div>
          <fieldset class="w-commerce-commercecheckoutblockcontent">
            <div class="w-commerce-commercecheckoutrow">
              <div class="w-commerce-commercecheckoutcolumn">
                <div class="w-commerce-commercecheckoutsummaryitem"><label class="w-commerce-commercecheckoutsummarylabel" data-udy-fe="text_3ff5b7c"><?php echo $udesly_fe_items['text_3ff5b7c'] ?></label>
                  <div></div>
                </div>
              </div>
              <div class="w-commerce-commercecheckoutcolumn">
                <div class="w-commerce-commercecheckoutsummaryitem"><label class="w-commerce-commercecheckoutsummarylabel" data-udy-fe="text_-35b49c7e"><?php echo $udesly_fe_items['text_-35b49c7e'] ?></label>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div class="w-commerce-commercecheckoutsummaryflexboxdiv">
                    <div class="w-commerce-commercecheckoutsummarytextspacingondiv"></div>
                    <div class="w-commerce-commercecheckoutsummarytextspacingondiv"></div>
                    <div class="w-commerce-commercecheckoutsummarytextspacingondiv"></div>
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
        <div class="w-commerce-commercecheckoutshippingsummarywrapper">
          <div class="w-commerce-commercecheckoutsummaryblockheader">
            <h4 data-udy-fe="text_12d3df93"><?php echo $udesly_fe_items['text_12d3df93'] ?></h4>
          </div>
          <fieldset class="w-commerce-commercecheckoutblockcontent">
            <div class="w-commerce-commercecheckoutrow">
              <div class="w-commerce-commercecheckoutcolumn">
                <div class="w-commerce-commercecheckoutsummaryitem">
                  <div></div>
                  <div></div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
        <div class="w-commerce-commercecheckoutpaymentsummarywrapper">
          <div class="w-commerce-commercecheckoutsummaryblockheader">
            <h4 data-udy-fe="text_-175ec138"><?php echo $udesly_fe_items['text_-175ec138'] ?></h4>
          </div>
          <fieldset class="w-commerce-commercecheckoutblockcontent">
            <div class="w-commerce-commercecheckoutrow">
              <div class="w-commerce-commercecheckoutcolumn">
                <div class="w-commerce-commercecheckoutsummaryitem"><label class="w-commerce-commercecheckoutsummarylabel" data-udy-fe="text_-175ec138"><?php echo $udesly_fe_items['text_-175ec138'] ?></label>
                  <div class="w-commerce-commercecheckoutsummaryflexboxdiv">
                    <div class="w-commerce-commercecheckoutsummarytextspacingondiv"></div>
                    <div class="w-commerce-commercecheckoutsummarytextspacingondiv"></div>
                  </div>
                  <div class="w-commerce-commercecheckoutsummaryflexboxdiv">
                    <div></div>
                    <div data-udy-fe="text_7df1"><?php echo $udesly_fe_items['text_7df1'] ?></div>
                    <div></div>
                  </div>
                </div>
              </div>
              <div class="w-commerce-commercecheckoutcolumn">
                <div class="w-commerce-commercecheckoutsummaryitem"><label class="w-commerce-commercecheckoutsummarylabel" data-udy-fe="text_789bff0f"><?php echo $udesly_fe_items['text_789bff0f'] ?></label>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div class="w-commerce-commercecheckoutsummaryflexboxdiv">
                    <div class="w-commerce-commercecheckoutsummarytextspacingondiv"></div>
                    <div class="w-commerce-commercecheckoutsummarytextspacingondiv"></div>
                    <div class="w-commerce-commercecheckoutsummarytextspacingondiv"></div>
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
        <div class="w-commerce-commercecheckoutorderitemswrapper">
          <div class="w-commerce-commercecheckoutsummaryblockheader">
            <h4 data-udy-fe="text_-3eedfb0d"><?php echo $udesly_fe_items['text_-3eedfb0d'] ?></h4>
          </div>
          <fieldset class="w-commerce-commercecheckoutblockcontent">
            <script type="text/x-wf-template" id="wf-template-5ea821aa8c4e75264e320443000000000051"></script>
            <div class="w-commerce-commercecheckoutorderitemslist" data-wf-collection="database.commerceOrder.userItems" data-wf-template-id="wf-template-5ea821aa8c4e75264e320443000000000051"></div>
          </fieldset>
        </div>
      </div>
      <div class="w-commerce-commercelayoutsidebar">
        <div class="w-commerce-commercecheckoutordersummarywrapper">
          <div class="w-commerce-commercecheckoutsummaryblockheader">
            <h4 data-udy-fe="text_-77079b6c"><?php echo $udesly_fe_items['text_-77079b6c'] ?></h4>
          </div>
          <fieldset class="w-commerce-commercecheckoutblockcontent">
            <div class="w-commerce-commercecheckoutsummarylineitem">
              <div data-udy-fe="text_-76f396dc"><?php echo $udesly_fe_items['text_-76f396dc'] ?></div>
              <div></div>
            </div>
            <script type="text/x-wf-template" id="wf-template-5ea821aa8c4e75264e32044300000000006b">%3Cdiv%20class%3D%22w-commerce-commercecheckoutordersummaryextraitemslistitem%22%3E%3Cdiv%3E%3C%2Fdiv%3E%3Cdiv%3E%3C%2Fdiv%3E%3C%2Fdiv%3E</script>
            <div class="w-commerce-commercecheckoutordersummaryextraitemslist" data-wf-collection="database.commerceOrder.extraItems" data-wf-template-id="wf-template-5ea821aa8c4e75264e32044300000000006b">
              <div class="w-commerce-commercecheckoutordersummaryextraitemslistitem">
                <div></div>
                <div></div>
              </div>
            </div>
            <div class="w-commerce-commercecheckoutsummarylineitem">
              <div data-udy-fe="text_4d3eb24"><?php echo $udesly_fe_items['text_4d3eb24'] ?></div>
              <div class="w-commerce-commercecheckoutsummarytotal"></div>
            </div>
          </fieldset>
        </div>
      </div>
    </div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1589866536765" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php udesly_set_fe_configuration($udesly_fe_items, 'order-confirmation'); ?></body></html>