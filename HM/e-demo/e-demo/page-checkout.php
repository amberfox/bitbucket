<!DOCTYPE html>
<html data-wf-page="5ebbe78b6291d65fc5cb2edf" data-wf-site="5ea7a66e99f15228d9bd936e"><head>
  <meta charset="utf-8">
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1589866536765" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1589866536765" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/e-demo.webflow.css?v=1589866536765" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Inconsolata:400,700","Pacifico:regular","Caveat:regular","Bree Serif:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1589866536765" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1589866536765" rel="apple-touch-icon">
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('checkout'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="checkout"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-node-type="commerce-checkout-form-container" data-wf-checkout-query="" data-wf-page-link-href-prefix="" class="w-commerce-commercecheckoutformcontainer">
    <div class="w-commerce-commercelayoutcontainer w-container"><?php udesly_wc_webflow_checkout('{"w":"w-commerce-commercelayoutcontainer w-container","c_w":"w-commerce-commercecheckoutcustomerinfowrapper","i":"w-commerce-commercecheckoutemailinput","o":"w-commerce-commercecheckoutshippingcountryselector","h":"w-commerce-commercecheckoutblockheader","l":"w-commerce-commercecheckoutlabel","m":"w-commerce-commercelayoutmain","s":"w-commerce-commercelayoutsidebar","c":"w-commerce-commercecheckoutblockcontent","header":"H4","header_c":"","l_i":"w-commerce-commercecheckoutsummarylineitem","b":"w-commerce-commercecheckoutplaceorderbutton checkout-btn"}'); ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1589866536765" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'checkout'); ?></body></html>