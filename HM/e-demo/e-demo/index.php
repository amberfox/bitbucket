<!DOCTYPE html>
<html data-wf-page="5ebbe78b6291d6f123cb2ed8" data-wf-site="5ea7a66e99f15228d9bd936e"><head>
  <meta charset="utf-8">
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1589866536765" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1589866536765" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/e-demo.webflow.css?v=1589866536765" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Inconsolata:400,700","Pacifico:regular","Caveat:regular","Bree Serif:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1589866536765" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1589866536765" rel="apple-touch-icon">
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('index'); ?></head>
<body class="<?php echo join(' ', get_body_class() ) . ' body'; ?>" udesly-page="index">
  <div class="header">
    <div class="header-nav w-container"><a id="w-node-e804e0340f3d-23cb2ed8" href="<?php echo $udesly_fe_items['link_23']; ?>" class="go-back w-inline-block" data-udy-fe="link_23"><img src="<?php echo $udesly_fe_items['image_-5b12c530']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_-5b12c530']->alt; ?>" class="arrow-icon" data-udy-fe="image_-5b12c530" srcset="<?php echo $udesly_fe_items['image_-5b12c530']->srcset; ?>"><p class="p-icon" data-udy-fe="text_42a3c91f"><?php echo $udesly_fe_items['text_42a3c91f'] ?></p></a><a id="w-node-393ccece92bf-23cb2ed8" href="<?php echo $udesly_fe_items['link_23']; ?>" class="share-header w-inline-block" data-udy-fe="link_23"><img src="<?php echo $udesly_fe_items['image_39343fbd']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_39343fbd']->alt; ?>" class="share-icon" data-udy-fe="image_39343fbd" srcset="<?php echo $udesly_fe_items['image_39343fbd']->srcset; ?>"><p class="p-icon" data-udy-fe="text_4c25fbf"><?php echo $udesly_fe_items['text_4c25fbf'] ?></p></a>
      <div data-node-type="commerce-cart-wrapper" id="w-node-2df5289485e9-23cb2ed8" data-open-product="" data-wf-cart-type="rightSidebar" data-wf-cart-query="" data-wf-page-link-href-prefix="" class="w-commerce-commercecartwrapper cart-header" href="<?php echo get_permalink( wc_get_page_id( 'cart' ) ); ?>"><a href="<?php echo $udesly_fe_items['link_23']; ?>" data-node-type="commerce-cart-open-link" class="w-commerce-commercecartopenlink cart-button-header w-inline-block" data-udy-fe="link_23"><img src="<?php echo $udesly_fe_items['image_-7a25d5c2']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_-7a25d5c2']->alt; ?>" class="cart-icon" data-udy-fe="image_-7a25d5c2" srcset="<?php echo $udesly_fe_items['image_-7a25d5c2']->srcset; ?>"><div class="w-commerce-commercecartopenlinkcount items-header" data-udy-fe="text_30"><?php echo $udesly_fe_items['text_30'] ?></div></a>
        <div data-node-type="commerce-cart-container-wrapper" style="display:none" class="w-commerce-commercecartcontainerwrapper w-commerce-commercecartcontainerwrapper--cartType-rightSidebar cart-wrapper">
          <div data-node-type="commerce-cart-container" class="w-commerce-commercecartcontainer">
            <div class="w-commerce-commercecartheader">
              <h4 class="w-commerce-commercecartheading" data-udy-fe="text_-77ee8913"><?php echo $udesly_fe_items['text_-77ee8913'] ?></h4><a href="<?php echo $udesly_fe_items['link_23']; ?>" data-node-type="commerce-cart-close-link" class="w-commerce-commercecartcloselink w-inline-block" data-udy-fe="link_23"><svg width="16px" height="16px" viewBox="0 0 16 16"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g fill-rule="nonzero" fill="#333333"><polygon points="6.23223305 8 0.616116524 13.6161165 2.38388348 15.3838835 8 9.76776695 13.6161165 15.3838835 15.3838835 13.6161165 9.76776695 8 15.3838835 2.38388348 13.6161165 0.616116524 8 6.23223305 2.38388348 0.616116524 0.616116524 2.38388348 6.23223305 8"></polygon></g></g></svg></a></div>
            <div class="w-commerce-commercecartformwrapper">
              <form data-node-type="commerce-cart-form" style="display:none" class="w-commerce-commercecartform">
                <ul role="list" class="mini-cart-list">
                  <li class="mini-cart-list-item"><img src="<?php echo $udesly_fe_items['image_317f733b']->src; ?>" alt="<?php echo $udesly_fe_items['image_317f733b']->alt; ?>" class="w-commerce-commercecartitemimage mini-cart-image" data-udy-fe="image_317f733b" srcset="<?php echo $udesly_fe_items['image_317f733b']->srcset; ?>">
                    <div class="mini-cart-product-name">
                      <div class="w-commerce-commercecartproductname cart-product-name" data-udy-fe="text_14a94844"><?php echo $udesly_fe_items['text_14a94844'] ?></div><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="remove-button w-button" data-udy-fe="text_4e5f412f,link_23"><?php echo $udesly_fe_items['text_4e5f412f'] ?></a></div>
                    <div class="price-qty">
                      <div data-udy-fe="text_34"><?php echo $udesly_fe_items['text_34'] ?></div>
                      <div class="cart-by" data-udy-fe="text_78"><?php echo $udesly_fe_items['text_78'] ?></div>
                      <div data-udy-fe="text_8d62"><?php echo $udesly_fe_items['text_8d62'] ?></div>
                    </div>
                  </li>
                </ul>
                <script type="text/x-wf-template" id="wf-template-07774d54-9299-60a0-b5de-2df5289485f8"></script>
                <div class="w-commerce-commercecartlist cart-list" data-wf-collection="database.commerceOrder.userItems" data-wf-template-id="wf-template-07774d54-9299-60a0-b5de-2df5289485f8"></div>
                <div class="w-commerce-commercecartfooter">
                  <div class="w-commerce-commercecartlineitem">
                    <div data-udy-fe="text_-76f396dc"><?php echo $udesly_fe_items['text_-76f396dc'] ?></div>
                    <div class="w-commerce-commercecartordervalue" data-udy-fe="text_8e1c"><?php echo $udesly_fe_items['text_8e1c'] ?></div>
                  </div>
                  <div>
                    <div data-node-type="commerce-cart-quick-checkout-actions">
                      <a data-node-type="commerce-cart-apple-pay-button" style="background-image:-webkit-named-image(apple-pay-logo-white);background-size:100% 50%;background-position:50% 50%;background-repeat:no-repeat" class="w-commerce-commercecartapplepaybutton">
                        <div></div>
                      </a><a data-node-type="commerce-cart-quick-checkout-button" style="display:none" class="w-commerce-commercecartquickcheckoutbutton"><svg class="w-commerce-commercequickcheckoutgoogleicon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16"><defs><polygon id="google-mark-a" points="0 .329 3.494 .329 3.494 7.649 0 7.649"></polygon><polygon id="google-mark-c" points=".894 0 13.169 0 13.169 6.443 .894 6.443"></polygon></defs><g fill="none" fill-rule="evenodd"><path fill="#4285F4" d="M10.5967,12.0469 L10.5967,14.0649 L13.1167,14.0649 C14.6047,12.6759 15.4577,10.6209 15.4577,8.1779 C15.4577,7.6339 15.4137,7.0889 15.3257,6.5559 L7.8887,6.5559 L7.8887,9.6329 L12.1507,9.6329 C11.9767,10.6119 11.4147,11.4899 10.5967,12.0469"></path><path fill="#34A853" d="M7.8887,16 C10.0137,16 11.8107,15.289 13.1147,14.067 C13.1147,14.066 13.1157,14.065 13.1167,14.064 L10.5967,12.047 C10.5877,12.053 10.5807,12.061 10.5727,12.067 C9.8607,12.556 8.9507,12.833 7.8887,12.833 C5.8577,12.833 4.1387,11.457 3.4937,9.605 L0.8747,9.605 L0.8747,11.648 C2.2197,14.319 4.9287,16 7.8887,16"></path><g transform="translate(0 4)"><mask id="google-mark-b" fill="#fff"><use xlink:href="#google-mark-a"></use></mask><path fill="#FBBC04" d="M3.4639,5.5337 C3.1369,4.5477 3.1359,3.4727 3.4609,2.4757 L3.4639,2.4777 C3.4679,2.4657 3.4749,2.4547 3.4789,2.4427 L3.4939,0.3287 L0.8939,0.3287 C0.8799,0.3577 0.8599,0.3827 0.8459,0.4117 C-0.2821,2.6667 -0.2821,5.3337 0.8459,7.5887 L0.8459,7.5997 C0.8549,7.6167 0.8659,7.6317 0.8749,7.6487 L3.4939,5.6057 C3.4849,5.5807 3.4729,5.5587 3.4639,5.5337" mask="url(#google-mark-b)"></path></g><mask id="google-mark-d" fill="#fff"><use xlink:href="#google-mark-c"></use></mask><path fill="#EA4335" d="M0.894,4.3291 L3.478,6.4431 C4.113,4.5611 5.843,3.1671 7.889,3.1671 C9.018,3.1451 10.102,3.5781 10.912,4.3671 L13.169,2.0781 C11.733,0.7231 9.85,-0.0219 7.889,0.0001 C4.941,0.0001 2.245,1.6791 0.894,4.3291" mask="url(#google-mark-d)"></path></g></svg><svg class="w-commerce-commercequickcheckoutmicrosofticon" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><g fill="none" fill-rule="evenodd"><polygon fill="#F05022" points="7 7 1 7 1 1 7 1"></polygon><polygon fill="#7DB902" points="15 7 9 7 9 1 15 1"></polygon><polygon fill="#00A4EE" points="7 15 1 15 1 9 7 9"></polygon><polygon fill="#FFB700" points="15 15 9 15 9 9 15 9"></polygon></g></svg><div data-udy-fe="text_-42873978"><?php echo $udesly_fe_items['text_-42873978'] ?></div></a></div><a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>" value="Continue to Checkout" data-node-type="cart-checkout-button" class="w-commerce-commercecartcheckoutbutton checkout-btn cart" data-loading-text="Hang Tight..." data-udy-fe="text_-7941186e"><?php echo $udesly_fe_items['text_-7941186e'] ?></a></div>
                </div>
              </form>
              <div class="w-commerce-commercecartemptystate">
                <div data-udy-fe="text_3d503d2b"><?php echo $udesly_fe_items['text_3d503d2b'] ?></div>
              </div>
              <div style="display:none" data-node-type="commerce-cart-error" class="w-commerce-commercecarterrorstate">
                <div class="w-cart-error-msg" data-w-cart-quantity-error="Product is not available in this quantity." data-w-cart-checkout-error="Checkout is disabled on this site." data-w-cart-general-error="Something went wrong when adding this item to the cart." data-w-cart-cart_order_min-error="The order minimum was not met. Add more items to your cart to continue." data-udy-fe="text_-1c765d0d"><?php echo $udesly_fe_items['text_-1c765d0d'] ?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="intro">
    <div class="wrapper-content">
      <div style="background-color:rgba(0,0,0,0)" class="owner">
        <h2 style="-webkit-transform:translate3d(0, -10VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, -10VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, -10VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, -10VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="welcome-text" data-udy-fe="text_-706a8be7"><?php echo $udesly_fe_items['text_-706a8be7'] ?></h2>
        <h2 style="opacity:0;-webkit-transform:translate3d(0, -40VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, -40VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, -40VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, -40VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="store-name" data-udy-fe="text_-55c3d956"><?php echo $udesly_fe_items['text_-55c3d956'] ?></h2>
        <h3 style="-webkit-transform:translate3d(-40VW, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-40VW, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-40VW, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-40VW, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="store-owner" data-udy-fe="text_-399547d5"><?php echo $udesly_fe_items['text_-399547d5'] ?></h3>
      </div>
      <div style="-webkit-transform:translate3d(0, 100VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 100VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 100VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 100VH, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="description">
        <h4 id="w-node-0e69dcad91d0-23cb2ed8" class="heading" data-udy-fe="text_-341840b6"><?php echo $udesly_fe_items['text_-341840b6'] ?></h4>
        <h2 id="w-node-bda14ae266cd-23cb2ed8" class="short-description" data-udy-fe="text_-4ed1b88f,text_7e41840d,text_1dfc8ee6"><?php echo $udesly_fe_items['text_-4ed1b88f'] ?><br><?php echo $udesly_fe_items['text_7e41840d'] ?><br><?php echo $udesly_fe_items['text_1dfc8ee6'] ?></h2>
      </div>
    </div>
  </div>
  <div class="products">
    <h2 class="h2-white center" data-udy-fe="text_14f86a44"><?php echo $udesly_fe_items['text_14f86a44'] ?></h2>
    <div class="wrapper-products w-container">
      <div id="w-node-6f650bad9c97-23cb2ed8" class="wrapper-ligthbox"><a href="<?php echo $udesly_fe_items['link_23']; ?>" id="w-node-d05a41209ea1-23cb2ed8" class="ligtbox w-inline-block w-lightbox" data-udy-fe="link_23"><img src="<?php echo $udesly_fe_items['image_-27374f1a']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-27374f1a']->srcset; ?>" sizes="(max-width: 479px) 54vw, (max-width: 767px) 49vw, (max-width: 991px) 356px, 462px" data-w-id="3c2b3e0d-1319-d9e9-eac3-b4ee897ffd6b" alt="<?php echo $udesly_fe_items['image_-27374f1a']->alt; ?>" class="img-hero-vert" data-udy-fe="image_-27374f1a"><script type="application/json" class="w-json">{"items":[{"width":1022,"caption":"Pandeyuca","height":1349,"fileName":"hero-pan_1.jpg","origFileName":"hero-pan_1.jpg","url":"<?php echo get_stylesheet_directory_uri(); ?>/images/hero-pan_1.jpg?v=1589866536765","_id":"5ea7eece6e337f82da5ae6e6","type":"image","fileSize":232329}],"group":"products"}</script></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" id="w-node-2722faabae32-23cb2ed8" class="ligtbox w-inline-block w-lightbox" data-udy-fe="link_23"><img src="<?php echo $udesly_fe_items['image_-26fef116']->src; ?>" data-w-id="89e53d49-09ca-edde-1aa2-2722faabae33" alt="<?php echo $udesly_fe_items['image_-26fef116']->alt; ?>" class="img-hero-vert" data-udy-fe="image_-26fef116" srcset="<?php echo $udesly_fe_items['image_-26fef116']->srcset; ?>"><script type="application/json" class="w-json">{"items":[{"width":620,"caption":"Mantecada","height":1102,"fileName":"hero-pan_5.jpg","origFileName":"hero-pan_5.jpg","url":"<?php echo get_stylesheet_directory_uri(); ?>/images/hero-pan_5.jpg?v=1589866536765","_id":"5ea8091ab67f875814556f77","type":"image","fileSize":87569}],"group":"products"}</script></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="ligtbox w-inline-block w-lightbox" data-udy-fe="link_23"><img src="<?php echo $udesly_fe_items['image_-26e2c214']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-26e2c214']->srcset; ?>" sizes="(max-width: 479px) 54vw, (max-width: 767px) 49vw, (max-width: 991px) 356px, 462px" data-w-id="8e393744-6f12-81d7-490a-6b4ac48d3b72" alt="<?php echo $udesly_fe_items['image_-26e2c214']->alt; ?>" class="img-hero-vert" data-udy-fe="image_-26e2c214"><script type="application/json" class="w-json">{"items":[{"width":901,"caption":"Pandeyuca","height":1600,"fileName":"hero-pan_6.jpg","origFileName":"hero-pan_6.jpg","url":"<?php echo get_stylesheet_directory_uri(); ?>/images/hero-pan_6.jpg?v=1589866536765","_id":"5ea8091bec431a0aa514de05","type":"image","fileSize":166196}],"group":"products"}</script></a></div>
      <div id="w-node-12aa5701b510-23cb2ed8" class="title-text">
        <div class="products-text-01"><img src="<?php echo $udesly_fe_items['image_25fd1456']->src; ?>" width="100" data-w-id="1521cb2f-44f5-f191-ceb3-95445143446a" alt="<?php echo $udesly_fe_items['image_25fd1456']->alt; ?>" data-udy-fe="image_25fd1456" srcset="<?php echo $udesly_fe_items['image_25fd1456']->srcset; ?>">
          <p data-w-id="d1b61fa8-b2c7-8198-ac1b-90c89564fb76" class="p p-products" data-udy-fe="text_2be60fd0"><?php echo $udesly_fe_items['text_2be60fd0'] ?><br></p><img src="<?php echo $udesly_fe_items['image_-506d48d5']->src; ?>" width="108" data-w-id="e88e57b8-edfa-ff7d-25d2-fd9a2638c266" alt="<?php echo $udesly_fe_items['image_-506d48d5']->alt; ?>" data-udy-fe="image_-506d48d5" srcset="<?php echo $udesly_fe_items['image_-506d48d5']->srcset; ?>">
          <p data-w-id="498bac90-75de-87af-9b55-ade2c6acb935" class="p p-products" data-udy-fe="text_32481cd3"><?php echo $udesly_fe_items['text_32481cd3'] ?><br></p><img src="<?php echo $udesly_fe_items['image_-4af79dc7']->src; ?>" width="109" data-w-id="59630550-0a72-4103-d3de-3f8bfa1ebad1" alt="<?php echo $udesly_fe_items['image_-4af79dc7']->alt; ?>" data-udy-fe="image_-4af79dc7" srcset="<?php echo $udesly_fe_items['image_-4af79dc7']->srcset; ?>">
          <p data-w-id="7cee39c8-7720-9be4-9973-9ae19b1089ea" class="p p-products" data-udy-fe="text_-40d40ef8"><?php echo $udesly_fe_items['text_-40d40ef8'] ?><br></p>
        </div>
      </div>
    </div>
  </div>
  <div data-w-id="83455719-b06c-f7d8-0472-4e9fedeee6ca" class="store">
    <h2 class="h2-white center" data-udy-fe="text_274f16"><?php echo $udesly_fe_items['text_274f16'] ?></h2>
    <div class="store w-container">
      <div class="usp-wrapper">
        <div id="w-node-28476d57cb89-6d57cb89" class="usp"><img src="<?php echo $udesly_fe_items['image_588bc54']->src; ?>" width="56" alt="<?php echo $udesly_fe_items['image_588bc54']->alt; ?>" class="img-usp" data-udy-fe="image_588bc54" srcset="<?php echo $udesly_fe_items['image_588bc54']->srcset; ?>">
          <p class="number-usp" data-udy-fe="text_69b"><?php echo $udesly_fe_items['text_69b'] ?></p>
          <p class="p-usp" data-udy-fe="text_3613b109"><?php echo $udesly_fe_items['text_3613b109'] ?></p>
        </div>
        <div id="w-node-28476d57cb89-6d57cb89" class="usp"><img src="<?php echo $udesly_fe_items['image_-506d48d5']->src; ?>" width="56" alt="<?php echo $udesly_fe_items['image_-506d48d5']->alt; ?>" class="img-usp" data-udy-fe="image_-506d48d5" srcset="<?php echo $udesly_fe_items['image_-506d48d5']->srcset; ?>">
          <p class="number-usp" data-udy-fe="text_61f"><?php echo $udesly_fe_items['text_61f'] ?><br></p>
          <p class="p-usp" data-udy-fe="text_-6ff5acb4"><?php echo $udesly_fe_items['text_-6ff5acb4'] ?><br></p>
        </div>
        <div id="w-node-28476d57cb89-6d57cb89" class="usp"><img src="<?php echo $udesly_fe_items['image_-4af79dc7']->src; ?>" width="56" alt="<?php echo $udesly_fe_items['image_-4af79dc7']->alt; ?>" class="img-usp" data-udy-fe="image_-4af79dc7" srcset="<?php echo $udesly_fe_items['image_-4af79dc7']->srcset; ?>">
          <p class="number-usp" data-udy-fe="text_61f"><?php echo $udesly_fe_items['text_61f'] ?><br></p>
          <p class="p-usp" data-udy-fe="text_7ba94079"><?php echo $udesly_fe_items['text_7ba94079'] ?></p>
        </div>
      </div>
      <div class="collection-list-wrapper w-dyn-list">
        <?php $products_number = 0; $the_query = udesly_get_content_query('featured-products'); ?><?php if ( $the_query->have_posts() ) : ?><div class="order-collection w-dyn-items" udy-el="wc-products" data-max-pages="<?php echo $the_query->max_num_pages ?>">
          <?php while( $the_query->have_posts() ) : $the_query->the_post(); global $post, $product; if ( !empty($product) && $product->is_visible() ) : $products_number++; ?><div class="<?php echo esc_attr( join( ' ', wc_get_product_class() ) ) . ' menu-item w-dyn-item'; ?>">
            <div class="store-product-wrapper"><img width="517" src="<?php echo udesly_woocommerce_featured_image_url('full') ?>" alt="<?php echo get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ); ?>" class="product-img" udy-el="wc-featured-image-card">
              <h3 class="store-product-name"><?php the_title(); ?></h3>
              <div class="product-price" udy-el="wc-price"><?php echo wc_price($product->get_price()); ?></div>
              <p class="p-product"><?php echo wpautop($product->get_short_description()); ?></p>
              <div class="add-to-cart" udy-el="wc-loop-add-to-cart">
                <form data-node-type="commerce-add-to-cart-form" class="w-commerce-commerceaddtocartform default-state" method="post" action="<?php echo esc_url( $product->add_to_cart_url() ) ?>" data-product-type="<?php echo "product_type_" . $product->get_type() ?>" data-product-stock="<?php echo $product->is_in_stock() ? "true" : "false"; ?>"><?php woocommerce_quantity_input(array('classes' => ["w-commerce-commerceaddtocartquantityinput","quantity"])); ?><input type="submit" data-loading-text="Adding to Basket..." value="<?php echo $product->add_to_cart_text(); ?>" class="w-commerce-commerceaddtocartbutton products-cta" data-product_id="<?php echo $product->get_id(); ?>" data-product_sku="<?php echo $product->get_sku(); ?>" aria-label="<?php echo $product->add_to_cart_description(); ?>"></form>
                <div style="<?php echo $product->is_in_stock() ? 'display: none;' : '' ?>" class="w-commerce-commerceaddtocartoutofstock" udy-el="wc-loop-out-of-stock">
                  <div data-udy-fe="text_7aaa2dac"><?php echo $udesly_fe_items['text_7aaa2dac'] ?></div>
                </div>
                <div data-node-type="commerce-add-to-cart-error" style="display:none" class="w-commerce-commerceaddtocarterror">
                  <div class="w-add-to-cart-error-msg" data-w-add-to-cart-buy-now-error="Something went wrong when trying to purchase this item." data-w-add-to-cart-checkout-disabled-error="Checkout is disabled on this site." udy-el="wc-loop-add-to-cart-error" data-udy-fe="text_-1c765d0d"><?php echo $udesly_fe_items['text_-1c765d0d'] ?></div>
                </div>
              </div>
            </div>
          </div><?php endif; endwhile; wp_reset_query(); ?>
        </div>
        <?php endif; if($products_number == 0) : ?><div class="w-dyn-empty">
          <div data-udy-fe="text_3d503d2b"><?php echo $udesly_fe_items['text_3d503d2b'] ?></div>
        </div><?php endif; ?>
      </div>
      <div data-duration-in="300" data-duration-out="100" class="tabs w-tabs">
        <div class="tab-menu-round w-tab-menu">
          <a data-w-tab="Category One" class="tab-link-round w-inline-block w-tab-link w--current">
            <div class="tab-text" data-udy-fe="text_231b2444"><?php echo $udesly_fe_items['text_231b2444'] ?></div>
          </a>
          <a data-w-tab="Category Two" class="tab-link-round w-inline-block w-tab-link">
            <div class="tab-text" data-udy-fe="text_231b382a"><?php echo $udesly_fe_items['text_231b382a'] ?></div>
          </a>
          <a data-w-tab="Category Three" class="tab-link-round w-inline-block w-tab-link">
            <div class="tab-text" data-udy-fe="text_-36d8e404"><?php echo $udesly_fe_items['text_-36d8e404'] ?></div>
          </a>
        </div>
        <div class="w-tab-content">
          <div data-w-tab="Category One" class="tab-pane-wrap w-tab-pane w--tab-active"></div>
          <div data-w-tab="Category Two" class="w-tab-pane">
            <div class="w-dyn-list">
              <div class="order-collection w-dyn-items w-row">
                <div class="menu-item w-dyn-item w-col w-col-6"></div>
              </div>
              <div class="w-dyn-empty">
                <div data-udy-fe="text_3d503d2b"><?php echo $udesly_fe_items['text_3d503d2b'] ?></div>
              </div>
            </div>
          </div>
          <div data-w-tab="Category Three" class="w-tab-pane">
            <div class="w-dyn-list">
              <div class="order-collection w-dyn-items w-row">
                <div class="menu-item w-dyn-item w-col w-col-6"></div>
              </div>
              <div class="w-dyn-empty">
                <div data-udy-fe="text_3d503d2b"><?php echo $udesly_fe_items['text_3d503d2b'] ?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div style="background-color:rgba(0,0,0,0)" class="owner">
    <div class="container-owner w-container">
      <div id="w-node-00fc4966cf39-23cb2ed8" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-267663f5']->src; ?>" id="w-node-00fc4966cf3c-23cb2ed8" alt="<?php echo $udesly_fe_items['image_-267663f5']->alt; ?>" class="hero-avatar" data-udy-fe="image_-267663f5" srcset="<?php echo $udesly_fe_items['image_-267663f5']->srcset; ?>">
        <h3 id="w-node-00fc4966cf3a-23cb2ed8" class="store-owner-2" data-udy-fe="text_-629f358c"><?php echo $udesly_fe_items['text_-629f358c'] ?></h3>
        <p id="w-node-00fc4966cf3f-23cb2ed8" class="p-hero" data-udy-fe="text_3f1ae3e3"><?php echo $udesly_fe_items['text_3f1ae3e3'] ?><br></p>
        <div id="w-node-76f449d4b1b6-23cb2ed8" class="hero-social-wrapper">
          <h6 class="hero-h6" data-udy-fe="text_42cccd7d"><?php echo $udesly_fe_items['text_42cccd7d'] ?></h6><img src="<?php echo $udesly_fe_items['image_-2712ac8']->src; ?>" alt="<?php echo $udesly_fe_items['image_-2712ac8']->alt; ?>" class="hero-social" data-udy-fe="image_-2712ac8" srcset="<?php echo $udesly_fe_items['image_-2712ac8']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_19eea9bf']->src; ?>" alt="<?php echo $udesly_fe_items['image_19eea9bf']->alt; ?>" class="hero-social" data-udy-fe="image_19eea9bf" srcset="<?php echo $udesly_fe_items['image_19eea9bf']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_-4cb31090']->src; ?>" alt="<?php echo $udesly_fe_items['image_-4cb31090']->alt; ?>" class="hero-social" data-udy-fe="image_-4cb31090" srcset="<?php echo $udesly_fe_items['image_-4cb31090']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_5d635e04']->src; ?>" alt="<?php echo $udesly_fe_items['image_5d635e04']->alt; ?>" class="hero-social" data-udy-fe="image_5d635e04" srcset="<?php echo $udesly_fe_items['image_5d635e04']->srcset; ?>"></div>
      </div>
    </div>
  </div>
  <div class="payment">
    <div class="container-payment w-container">
      <div id="w-node-55f3e32dc577-23cb2ed8" class="secured"><img src="<?php echo $udesly_fe_items['image_-68c2148f']->src; ?>" width="177" height="49" alt="<?php echo $udesly_fe_items['image_-68c2148f']->alt; ?>" class="secured-logo" data-udy-fe="image_-68c2148f" srcset="<?php echo $udesly_fe_items['image_-68c2148f']->srcset; ?>">
        <h2 class="payment-h2" data-udy-fe="text_5b85bf38"><?php echo $udesly_fe_items['text_5b85bf38'] ?></h2>
      </div>
      <p id="w-node-c5db5ec990dd-23cb2ed8" class="p-payment" data-udy-fe="text_10bfd475,text_200d"><?php echo $udesly_fe_items['text_10bfd475'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br></p>
      <div id="w-node-8025a1d839b8-23cb2ed8" class="payment-logos"><img src="<?php echo $udesly_fe_items['image_-1d25df1d']->src; ?>" width="50" height="50" alt="<?php echo $udesly_fe_items['image_-1d25df1d']->alt; ?>" class="logo-payment" data-udy-fe="image_-1d25df1d" srcset="<?php echo $udesly_fe_items['image_-1d25df1d']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_790a3071']->src; ?>" width="50" height="50" alt="<?php echo $udesly_fe_items['image_790a3071']->alt; ?>" class="logo-payment" data-udy-fe="image_790a3071" srcset="<?php echo $udesly_fe_items['image_790a3071']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_-1146939e']->src; ?>" width="50" height="50" alt="<?php echo $udesly_fe_items['image_-1146939e']->alt; ?>" class="logo-payment" data-udy-fe="image_-1146939e" srcset="<?php echo $udesly_fe_items['image_-1146939e']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_-4840d8ef']->src; ?>" width="50" height="50" alt="<?php echo $udesly_fe_items['image_-4840d8ef']->alt; ?>" class="logo-payment" data-udy-fe="image_-4840d8ef" srcset="<?php echo $udesly_fe_items['image_-4840d8ef']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_-333d5140']->src; ?>" width="50" height="50" alt="<?php echo $udesly_fe_items['image_-333d5140']->alt; ?>" class="logo-payment" data-udy-fe="image_-333d5140" srcset="<?php echo $udesly_fe_items['image_-333d5140']->srcset; ?>"></div>
    </div>
  </div>
  <div data-w-id="cad40940-a525-c4b1-d5d4-c0095dbe5226" style="display:none" class="checkout-section">
    <div class="container-checkout-yes w-container">
      <p data-w-id="3e8400c1-3ef8-62d7-530a-8b83594667bf" class="p-footer" data-udy-fe="text_-37593723"><?php echo $udesly_fe_items['text_-37593723'] ?></p><a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>" class="checkout-btn w-button" data-udy-fe="text_-7941186e"><?php echo $udesly_fe_items['text_-7941186e'] ?></a></div>
  </div>
  <div data-w-id="8cc0c9b1-5d9a-680f-cead-4dddfe25d054" class="footer"></div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1589866536765" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php udesly_set_fe_configuration($udesly_fe_items, 'index'); ?></body></html>