<!DOCTYPE html>
<?php /*
        Template Name: young-lineage
        */ ?> 
        <html data-wf-page="5ed10ec3d2b21c19ae269f45" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  
  
  <meta content="Young Lineage" property="twitter:title">
  <meta content="In my service as a barber I offer a good cut, beard and eyebrow service. I also offer wax hair care products." property="twitter:description">
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590972251708" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590972251708" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590972251708" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.gif?v=1590972251708" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.jpg?v=1590972251708" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('young-lineage'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="young-lineage"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_4eed19bd']->src; ?>" width="125" alt="<?php echo $udesly_fe_items['image_4eed19bd']->alt; ?>" class="ourtales-logo" data-udy-fe="image_4eed19bd" srcset="<?php echo $udesly_fe_items['image_4eed19bd']->srcset; ?>"></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="tales-basic">
    <div data-animation="slide" data-duration="500" data-infinite="1" class="tales-basic-slider w-slider">
      <div class="w-slider-mask">
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_7e035d03']->src; ?>" alt="<?php echo $udesly_fe_items['image_7e035d03']->alt; ?>" class="hero-slide-img" data-udy-fe="image_7e035d03" srcset="<?php echo $udesly_fe_items['image_7e035d03']->srcset; ?>"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_7e117484']->src; ?>" srcset="<?php echo $udesly_fe_items['image_7e117484']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_7e117484']->alt; ?>" class="hero-slide-img" data-udy-fe="image_7e117484"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper buttom"><img src="<?php echo $udesly_fe_items['image_-2f3c0e9a']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-2f3c0e9a']->srcset; ?>" sizes="(max-width: 700px) 100vw, (max-width: 991px) 700px, 940px" alt="<?php echo $udesly_fe_items['image_-2f3c0e9a']->alt; ?>" class="hero-slide-img" data-udy-fe="image_-2f3c0e9a"></div>
        </div>
      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav w-round"></div>
    </div>
  </div>
  <div class="intro">
    <div class="page-container w-container">
      <div class="ambrosia-logo-cont"><img src="<?php echo $udesly_fe_items['image_5faf94ac']->src; ?>" alt="<?php echo $udesly_fe_items['image_5faf94ac']->alt; ?>" class="round-logo" data-udy-fe="image_5faf94ac" srcset="<?php echo $udesly_fe_items['image_5faf94ac']->srcset; ?>"></div>
      <div class="text-cont">
        <h1 class="h1-young-lineage" data-udy-fe="text_-5ba47f21"><?php echo $udesly_fe_items['text_-5ba47f21'] ?></h1>
        <h2 class="h2-young-lineage" data-udy-fe="text_3eca93f2"><?php echo $udesly_fe_items['text_3eca93f2'] ?></h2>
      </div>
    </div>
  </div>
  <div class="products young-lineage">
    <div class="page-container _3 w-container">
      <div class="product-wrapper">
        <div id="w-node-50d50a3bd31d-ae269f45" class="product-desc">
          <h2 class="h2-young-lineage" data-udy-fe="text_6ea23547"><?php echo $udesly_fe_items['text_6ea23547'] ?><a href="<?php echo $udesly_fe_items['link_4ab1a51b']; ?>" data-udy-fe="link_4ab1a51b"><strong><br></strong></a></h2>
          <p data-udy-fe="text_7bc111e3"><?php echo $udesly_fe_items['text_7bc111e3'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd326-ae269f45" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_7e2da386']->src; ?>" srcset="<?php echo $udesly_fe_items['image_7e2da386']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_7e2da386']->alt; ?>" class="img-product" data-udy-fe="image_7e2da386"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_7e1f8c05']->src; ?>" srcset="<?php echo $udesly_fe_items['image_7e1f8c05']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_7e1f8c05']->alt; ?>" class="img-product" data-udy-fe="image_7e1f8c05"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_5faf94ac']->src; ?>" width="261" alt="<?php echo $udesly_fe_items['image_5faf94ac']->alt; ?>" class="ico-round-shadow" data-udy-fe="image_5faf94ac" srcset="<?php echo $udesly_fe_items['image_5faf94ac']->srcset; ?>"></div>
    <div class="text-cont _3">
      <h1 class="h1-young-lineage _3" data-udy-fe="text_90b3967,text_6edf839f"><?php echo $udesly_fe_items['text_90b3967'] ?><br><?php echo $udesly_fe_items['text_6edf839f'] ?></h1>
      <h2 class="h2-young-lineage" data-udy-fe="text_-7a1a8d1f"><?php echo $udesly_fe_items['text_-7a1a8d1f'] ?></h2><a href="<?php echo $udesly_fe_items['link_394f613']; ?>" class="young-lineage-btn w-button" data-udy-fe="text_-6d09b137,link_394f613"><?php echo $udesly_fe_items['text_-6d09b137'] ?></a></div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-4e99a226']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-4e99a226']->alt; ?>" sizes="80px" srcset="<?php echo $udesly_fe_items['image_-4e99a226']->srcset; ?>" class="hero-avatar" data-udy-fe="image_-4e99a226">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_3fc16fe5"><?php echo $udesly_fe_items['text_3fc16fe5'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_-1e3a46ec"><?php echo $udesly_fe_items['text_-1e3a46ec'] ?></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_-ef996c2"><?php echo $udesly_fe_items['text_-ef996c2'] ?></h6><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social facebook w-inline-block" data-udy-fe="link_23"></a><a href="<?php echo $udesly_fe_items['link_4ab1a51b']; ?>" target="_blank" class="hero-social insta w-inline-block" data-udy-fe="link_4ab1a51b"></a><a href="<?php echo $udesly_fe_items['link_394f613']; ?>" class="hero-social whatsapp w-inline-block" data-udy-fe="link_394f613"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_645efc51"><?php echo $udesly_fe_items['text_645efc51'] ?></h1>
        <p class="p-footer" data-udy-fe="text_553e063d"><?php echo $udesly_fe_items['text_553e063d'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <h1 class="heading h3-footer" data-udy-fe="text_c4f6bc3"><?php echo $udesly_fe_items['text_c4f6bc3'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-7740163c"><?php echo $udesly_fe_items['text_-7740163c'] ?></h1>
        <p class="p-footer" data-udy-fe="text_44494aa"><?php echo $udesly_fe_items['text_44494aa'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590972251708" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <style data-udy-fe="text_-2b41b03c"><?php echo $udesly_fe_items['text_-2b41b03c'] ?></style>

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'young-lineage'); ?></body></html>