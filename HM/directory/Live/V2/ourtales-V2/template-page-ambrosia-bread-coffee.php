<!DOCTYPE html>
<?php /*
        Template Name: ambrosia-bread-coffee
        */ ?> 
        <html data-wf-page="5ece1de493cf0410bb3acc67" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  
  
  <meta content="Ambrosia Bread" property="twitter:title">
  <meta content="Roscones / Pan de queso / Pan blando / Pan integral / Galletas de vainilla / Corazones de hojaldre / Mantecada / Sweet Bread / Cheese Bread / Vanilla Cookies / Patisserie" property="twitter:description">
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590972251708" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590972251708" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590972251708" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.gif?v=1590972251708" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.jpg?v=1590972251708" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('ambrosia-bread-coffee'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="ambrosia-bread-coffee"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_4eed19bd']->src; ?>" width="125" alt="<?php echo $udesly_fe_items['image_4eed19bd']->alt; ?>" class="ourtales-logo" data-udy-fe="image_4eed19bd" srcset="<?php echo $udesly_fe_items['image_4eed19bd']->srcset; ?>"></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="tales-basic">
    <div data-animation="slide" data-duration="500" data-infinite="1" class="tales-basic-slider w-slider">
      <div class="w-slider-mask">
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper buttom"><img src="<?php echo $udesly_fe_items['image_3991763']->src; ?>" alt="<?php echo $udesly_fe_items['image_3991763']->alt; ?>" class="hero-slide-img" data-udy-fe="image_3991763" srcset="<?php echo $udesly_fe_items['image_3991763']->srcset; ?>"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_5b09489']->src; ?>" alt="<?php echo $udesly_fe_items['image_5b09489']->alt; ?>" class="hero-slide-img" data-udy-fe="image_5b09489" srcset="<?php echo $udesly_fe_items['image_5b09489']->srcset; ?>"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_-285e8cf7']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-285e8cf7']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_-285e8cf7']->alt; ?>" class="hero-slide-img" data-udy-fe="image_-285e8cf7"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_-26a9b458']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-26a9b458']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_-26a9b458']->alt; ?>" class="hero-slide-img" data-udy-fe="image_-26a9b458"></div>
        </div>
      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav w-round"></div>
    </div>
  </div>
  <div class="intro">
    <div class="page-container w-container">
      <div class="ambrosia-logo-cont"><img src="<?php echo $udesly_fe_items['image_38affe2']->src; ?>" srcset="<?php echo $udesly_fe_items['image_38affe2']->srcset; ?>" sizes="100vw" alt="<?php echo $udesly_fe_items['image_38affe2']->alt; ?>" class="round-logo" data-udy-fe="image_38affe2"></div>
      <div class="text-cont">
        <h1 class="h1-ambrosia" data-udy-fe="text_-38b8ed8a"><?php echo $udesly_fe_items['text_-38b8ed8a'] ?></h1>
        <h2 class="h2-ambrosia" data-udy-fe="text_7a51190"><?php echo $udesly_fe_items['text_7a51190'] ?></h2>
        <p data-udy-fe="text_3637e347"><?php echo $udesly_fe_items['text_3637e347'] ?><br></p>
      </div>
    </div>
  </div>
  <div class="products ambrosia">
    <div class="page-container _3 w-container">
      <h1 class="h1-ambrosia _2" data-udy-fe="text_-7a5fd008"><?php echo $udesly_fe_items['text_-7a5fd008'] ?></h1>
      <div class="product-wrapper">
        <div id="w-node-50d50a3bd31d-bb3acc67" class="product-desc">
          <h2 class="h2-ambrosia" data-udy-fe="text_-685de0f6,text_3e572749"><?php echo $udesly_fe_items['text_-685de0f6'] ?><br><?php echo $udesly_fe_items['text_3e572749'] ?></h2>
          <p data-udy-fe="text_66245421"><?php echo $udesly_fe_items['text_66245421'] ?><br></p>
          <p class="price" data-udy-fe="text_5a182174"><?php echo $udesly_fe_items['text_5a182174'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd326-bb3acc67" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_3c35de6']->src; ?>" srcset="<?php echo $udesly_fe_items['image_3c35de6']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 83vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_3c35de6']->alt; ?>" class="img-product" data-udy-fe="image_3c35de6"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_3d17567']->src; ?>" srcset="<?php echo $udesly_fe_items['image_3d17567']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 83vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_3d17567']->alt; ?>" class="img-product" data-udy-fe="image_3d17567"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-50d50a3bd334-bb3acc67" class="product-desc">
          <h1 class="h2-ambrosia" data-udy-fe="text_6ac5f5fd"><?php echo $udesly_fe_items['text_6ac5f5fd'] ?></h1>
          <p data-udy-fe="text_5da68fa3"><?php echo $udesly_fe_items['text_5da68fa3'] ?><strong></strong><br></p>
          <p class="price" data-udy-fe="text_44d33812"><?php echo $udesly_fe_items['text_44d33812'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd355-bb3acc67" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_3df8ce8']->src; ?>" srcset="<?php echo $udesly_fe_items['image_3df8ce8']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 84vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_3df8ce8']->alt; ?>" class="img-product" data-udy-fe="image_3df8ce8"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_3eda469']->src; ?>" srcset="<?php echo $udesly_fe_items['image_3eda469']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 84vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_3eda469']->alt; ?>" class="img-product" data-udy-fe="image_3eda469"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-7e24dca5a4a4-bb3acc67" class="product-desc">
          <h2 class="h2-ambrosia" data-udy-fe="text_-4fcc89d4"><?php echo $udesly_fe_items['text_-4fcc89d4'] ?></h2>
          <p data-udy-fe="text_6b4ab0a0"><?php echo $udesly_fe_items['text_6b4ab0a0'] ?><br></p>
          <p class="price" data-udy-fe="text_-42d2b568"><?php echo $udesly_fe_items['text_-42d2b568'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-7e24dca5a4af-bb3acc67" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_3fbbbea']->src; ?>" srcset="<?php echo $udesly_fe_items['image_3fbbbea']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 84vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_3fbbbea']->alt; ?>" class="img-product" data-udy-fe="image_3fbbbea"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_531c100']->src; ?>" srcset="<?php echo $udesly_fe_items['image_531c100']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 84vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_531c100']->alt; ?>" class="img-product" data-udy-fe="image_531c100"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-d3c2225ec374-bb3acc67" class="product-desc">
          <h1 class="h2-ambrosia" data-udy-fe="text_637c8fc0"><?php echo $udesly_fe_items['text_637c8fc0'] ?></h1>
          <p data-udy-fe="text_3afd206c"><?php echo $udesly_fe_items['text_3afd206c'] ?><strong></strong><br></p>
          <p class="price" data-udy-fe="text_-6abb4faa"><?php echo $udesly_fe_items['text_-6abb4faa'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-d3c2225ec37f-bb3acc67" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_72d0f24']->src; ?>" srcset="<?php echo $udesly_fe_items['image_72d0f24']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 84vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_72d0f24']->alt; ?>" class="img-product" data-udy-fe="image_72d0f24"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_53fd881']->src; ?>" srcset="<?php echo $udesly_fe_items['image_53fd881']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 84vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_53fd881']->alt; ?>" class="img-product" data-udy-fe="image_53fd881"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-bc4c951a619e-bb3acc67" class="product-desc">
          <h2 class="h2-ambrosia" data-udy-fe="text_4acbb1a3"><?php echo $udesly_fe_items['text_4acbb1a3'] ?></h2>
          <p data-udy-fe="text_-32af01d1"><?php echo $udesly_fe_items['text_-32af01d1'] ?><br></p>
          <p class="price" data-udy-fe="text_-7dac181e"><?php echo $udesly_fe_items['text_-7dac181e'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-bc4c951a61a7-bb3acc67" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_73b26a5']->src; ?>" srcset="<?php echo $udesly_fe_items['image_73b26a5']->srcset; ?>" sizes="(max-width: 767px) 79vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_73b26a5']->alt; ?>" class="img-product" data-udy-fe="image_73b26a5"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_55c0783']->src; ?>" srcset="<?php echo $udesly_fe_items['image_55c0783']->srcset; ?>" sizes="(max-width: 767px) 79vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_55c0783']->alt; ?>" class="img-product" data-udy-fe="image_55c0783"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-1d7a3e17f7ba-bb3acc67" class="product-desc">
          <h1 class="h2-ambrosia" data-udy-fe="text_fc53723,text_2ee5a325,text_-5f767ad0"><?php echo $udesly_fe_items['text_fc53723'] ?><br><?php echo $udesly_fe_items['text_2ee5a325'] ?><br><?php echo $udesly_fe_items['text_-5f767ad0'] ?></h1>
          <p data-udy-fe="text_7db20507"><?php echo $udesly_fe_items['text_7db20507'] ?><strong></strong><br></p>
          <p class="price" data-udy-fe="text_-2dfafdeb"><?php echo $udesly_fe_items['text_-2dfafdeb'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-1d7a3e17f7c5-bb3acc67" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5783685']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5783685']->srcset; ?>" sizes="(max-width: 479px) 59vw, (max-width: 767px) 249.3125px, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_5783685']->alt; ?>" class="img-product" data-udy-fe="image_5783685"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5864e06']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5864e06']->srcset; ?>" sizes="(max-width: 479px) 59vw, (max-width: 767px) 249.3125px, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_5864e06']->alt; ?>" class="img-product" data-udy-fe="image_5864e06"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-81814e1ce9f5-bb3acc67" class="product-desc">
          <h2 class="h2-ambrosia" data-udy-fe="text_206010af,text_5939be3f,text_-5f767ad0"><?php echo $udesly_fe_items['text_206010af'] ?><br><?php echo $udesly_fe_items['text_5939be3f'] ?><br><?php echo $udesly_fe_items['text_-5f767ad0'] ?></h2>
          <p data-udy-fe="text_21b91a86"><?php echo $udesly_fe_items['text_21b91a86'] ?><br></p>
          <p class="price" data-udy-fe="text_-39b18ccc"><?php echo $udesly_fe_items['text_-39b18ccc'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-81814e1ce9fe-bb3acc67" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5a27d08']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5a27d08']->srcset; ?>" sizes="(max-width: 479px) 59vw, (max-width: 767px) 189.484375px, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_5a27d08']->alt; ?>" class="img-product" data-udy-fe="image_5a27d08"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5946587']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5946587']->srcset; ?>" sizes="(max-width: 479px) 59vw, (max-width: 767px) 189.484375px, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_5946587']->alt; ?>" class="img-product" data-udy-fe="image_5946587"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-733bb6132cda-bb3acc67" class="product-desc">
          <h1 class="h2-ambrosia" data-udy-fe="text_-78e61819"><?php echo $udesly_fe_items['text_-78e61819'] ?></h1>
          <p data-udy-fe="text_-6b25f0e5"><?php echo $udesly_fe_items['text_-6b25f0e5'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-733bb6132ce9-bb3acc67" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_702c8a1']->src; ?>" srcset="<?php echo $udesly_fe_items['image_702c8a1']->srcset; ?>" sizes="(max-width: 479px) 59vw, (max-width: 767px) 214.96875px, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_702c8a1']->alt; ?>" class="img-product" data-udy-fe="image_702c8a1"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_710e022']->src; ?>" alt="<?php echo $udesly_fe_items['image_710e022']->alt; ?>" class="img-product" data-udy-fe="image_710e022" srcset="<?php echo $udesly_fe_items['image_710e022']->srcset; ?>"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_71ef7a3']->src; ?>" srcset="<?php echo $udesly_fe_items['image_71ef7a3']->srcset; ?>" sizes="(max-width: 479px) 59vw, (max-width: 767px) 214.96875px, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_71ef7a3']->alt; ?>" class="img-product" data-udy-fe="image_71ef7a3"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_38affe2']->src; ?>" width="200" srcset="<?php echo $udesly_fe_items['image_38affe2']->srcset; ?>" sizes="(max-width: 479px) 200px, (max-width: 767px) 34vw, 200px" alt="<?php echo $udesly_fe_items['image_38affe2']->alt; ?>" data-udy-fe="image_38affe2"></div>
    <div class="text-cont _3">
      <h1 class="h1-ambrosia _3" data-udy-fe="text_2efa875a,text_-3a34529e"><?php echo $udesly_fe_items['text_2efa875a'] ?><br><?php echo $udesly_fe_items['text_-3a34529e'] ?></h1>
      <h2 class="h2-ambrosia" data-udy-fe="text_51c16288"><?php echo $udesly_fe_items['text_51c16288'] ?></h2>
      <p data-udy-fe="text_3a99d6c3"><?php echo $udesly_fe_items['text_3a99d6c3'] ?><strong></strong><br></p><a href="<?php echo $udesly_fe_items['link_-589fbf94']; ?>" class="ambrosia-btn w-button" data-udy-fe="text_-5fa13623,link_-589fbf94"><?php echo $udesly_fe_items['text_-5fa13623'] ?></a></div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-5f24ead4']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-5f24ead4']->alt; ?>" class="hero-avatar" data-udy-fe="image_-5f24ead4" srcset="<?php echo $udesly_fe_items['image_-5f24ead4']->srcset; ?>">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_-167fe47"><?php echo $udesly_fe_items['text_-167fe47'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_1d6641ea"><?php echo $udesly_fe_items['text_1d6641ea'] ?><br></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_65c19510"><?php echo $udesly_fe_items['text_65c19510'] ?></h6><a href="<?php echo $udesly_fe_items['link_-3848aa29']; ?>" class="hero-social facebook w-inline-block" data-udy-fe="link_-3848aa29"></a><a href="<?php echo $udesly_fe_items['link_-4f52d2cd']; ?>" class="hero-social insta w-inline-block" data-udy-fe="link_-4f52d2cd"></a><a href="<?php echo $udesly_fe_items['link_-589fbf94']; ?>" class="hero-social whatsapp w-inline-block" data-udy-fe="link_-589fbf94"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_645efc51"><?php echo $udesly_fe_items['text_645efc51'] ?></h1>
        <p class="p-footer" data-udy-fe="text_553e063d"><?php echo $udesly_fe_items['text_553e063d'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <h1 class="heading h3-footer" data-udy-fe="text_c4f6bc3"><?php echo $udesly_fe_items['text_c4f6bc3'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-7740163c"><?php echo $udesly_fe_items['text_-7740163c'] ?></h1>
        <p class="p-footer" data-udy-fe="text_44494aa"><?php echo $udesly_fe_items['text_44494aa'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590972251708" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <style data-udy-fe="text_20e2f9b2"><?php echo $udesly_fe_items['text_20e2f9b2'] ?></style>

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'ambrosia-bread-coffee'); ?></body></html>