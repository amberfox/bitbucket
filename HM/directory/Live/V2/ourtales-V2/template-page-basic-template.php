<!DOCTYPE html>
<?php /*
        Template Name: basic-template
        */ ?> 
        <html data-wf-page="5ecdae22bb878db454b88248" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  <meta content="Basic Template" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590972251708" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590972251708" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590972251708" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.gif?v=1590972251708" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.jpg?v=1590972251708" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('basic-template'); ?></head>
<body class="<?php echo join(' ', get_body_class() ) . ' body'; ?>" udesly-page="basic-template"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_4eed19bd']->src; ?>" width="125" alt="<?php echo $udesly_fe_items['image_4eed19bd']->alt; ?>" class="ourtales-logo" data-udy-fe="image_4eed19bd" srcset="<?php echo $udesly_fe_items['image_4eed19bd']->srcset; ?>"></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="welcome">
    <div class="w-container">
      <div class="left-mini-section">
        <h1 class="heading"><?php the_title(); ?></h1>
        <?php the_content(); ?>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_645efc51"><?php echo $udesly_fe_items['text_645efc51'] ?></h1>
        <p class="p-footer" data-udy-fe="text_553e063d"><?php echo $udesly_fe_items['text_553e063d'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <h1 class="heading h3-footer" data-udy-fe="text_c4f6bc3"><?php echo $udesly_fe_items['text_c4f6bc3'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-7740163c"><?php echo $udesly_fe_items['text_-7740163c'] ?></h1>
        <p class="p-footer" data-udy-fe="text_44494aa"><?php echo $udesly_fe_items['text_44494aa'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590972251708" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'basic-template'); ?></body></html>