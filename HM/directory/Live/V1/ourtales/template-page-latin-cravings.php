<!DOCTYPE html>
<?php /*
        Template Name: latin-cravings
        */ ?> 
        <html data-wf-page="5ed1d762ef82aeb6cb798056" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  
  
  <meta content="Latin Cravings" property="twitter:title">
  <meta content="Papas Rellenas - Stuffed potatoes /  Arepas /  Pan de Bonos / Empanadas / Dishes made by Request / Vegetarian Options" property="twitter:description">
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590918372690" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590918372690" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590918372690" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.gif?v=1590918372690" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.jpg?v=1590918372690" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('latin-cravings'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="latin-cravings"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_4eed19bd']->src; ?>" width="125" alt="<?php echo $udesly_fe_items['image_4eed19bd']->alt; ?>" class="ourtales-logo" data-udy-fe="image_4eed19bd" srcset="<?php echo $udesly_fe_items['image_4eed19bd']->srcset; ?>"></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="tales-basic">
    <div data-animation="slide" data-duration="500" data-infinite="1" class="tales-basic-slider w-slider">
      <div class="w-slider-mask">
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper buttom"><img src="<?php echo $udesly_fe_items['image_-a6cef90']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-a6cef90']->srcset; ?>" sizes="(max-width: 960px) 100vw, (max-width: 991px) 960px, 940px" alt="<?php echo $udesly_fe_items['image_-a6cef90']->alt; ?>" class="hero-slide-img" data-udy-fe="image_-a6cef90"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_-a7b0711']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-a7b0711']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_-a7b0711']->alt; ?>" class="hero-slide-img" data-udy-fe="image_-a7b0711"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_-aa54d94']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-aa54d94']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_-aa54d94']->alt; ?>" class="hero-slide-img" data-udy-fe="image_-aa54d94"></div>
        </div>
      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav w-round"></div>
    </div>
  </div>
  <div class="intro">
    <div class="page-container w-container">
      <div class="ambrosia-logo-cont"><img src="<?php echo $udesly_fe_items['image_-6c9290ea']->src; ?>" width="199" alt="<?php echo $udesly_fe_items['image_-6c9290ea']->alt; ?>" class="round-logo" data-udy-fe="image_-6c9290ea" srcset="<?php echo $udesly_fe_items['image_-6c9290ea']->srcset; ?>"></div>
      <div class="text-cont">
        <h1 class="h1-latin-cravings" data-udy-fe="text_-689bd2ae"><?php echo $udesly_fe_items['text_-689bd2ae'] ?></h1>
        <h2 class="h2-latin-cravings" data-udy-fe="text_-6ac1b139"><?php echo $udesly_fe_items['text_-6ac1b139'] ?></h2>
        <p data-udy-fe="text_-11c644f3"><?php echo $udesly_fe_items['text_-11c644f3'] ?><br></p>
      </div>
    </div>
  </div>
  <div class="products latin-cravings">
    <div class="page-container _3 w-container">
      <h1 class="h1-latin-cravings _2" data-udy-fe="text_-7a5fd008"><?php echo $udesly_fe_items['text_-7a5fd008'] ?></h1>
      <div class="product-wrapper">
        <div id="w-node-50d50a3bd31d-cb798056" class="product-desc">
          <h2 class="h2-latin-cravings" data-udy-fe="text_123a7b42,text_-4701784"><?php echo $udesly_fe_items['text_123a7b42'] ?><br><?php echo $udesly_fe_items['text_-4701784'] ?></h2>
          <p data-udy-fe="text_63b8c872"><?php echo $udesly_fe_items['text_63b8c872'] ?><br></p>
        </div>
        <div id="w-node-50d50a3bd329-cb798056" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-779335f']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-779335f']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_-779335f']->alt; ?>" class="img-product" data-udy-fe="image_-779335f"></div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-e76c37e5c755-cb798056" class="product-desc">
          <h2 class="h2-latin-cravings" data-udy-fe="text_54671537"><?php echo $udesly_fe_items['text_54671537'] ?></h2>
          <p data-udy-fe="text_-470b5c94"><?php echo $udesly_fe_items['text_-470b5c94'] ?><br></p>
        </div>
        <div id="w-node-e76c37e5c760-cb798056" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-a891e92']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-a891e92']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_-a891e92']->alt; ?>" class="img-product" data-udy-fe="image_-a891e92"></div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-9567b3179cd5-cb798056" class="product-desc">
          <h2 class="h2-latin-cravings" data-udy-fe="text_23cf586f,text_43fd1307"><?php echo $udesly_fe_items['text_23cf586f'] ?><br><?php echo $udesly_fe_items['text_43fd1307'] ?></h2>
          <p data-udy-fe="text_-412066bb"><?php echo $udesly_fe_items['text_-412066bb'] ?><br></p>
        </div>
        <div id="w-node-9567b3179cdd-cb798056" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-aa54d94']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-aa54d94']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_-aa54d94']->alt; ?>" class="img-product" data-udy-fe="image_-aa54d94"></div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-0c3fe3fc3bb3-cb798056" class="product-desc">
          <h2 class="h2-latin-cravings" data-udy-fe="text_1cdbbea"><?php echo $udesly_fe_items['text_1cdbbea'] ?></h2>
          <p data-udy-fe="text_-319410f0"><?php echo $udesly_fe_items['text_-319410f0'] ?><br></p>
        </div>
        <div id="w-node-0c3fe3fc3bb9-cb798056" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-a973613']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-a973613']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_-a973613']->alt; ?>" class="img-product" data-udy-fe="image_-a973613"></div>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_-a5ed80f']->src; ?>" width="200" srcset="<?php echo $udesly_fe_items['image_-a5ed80f']->srcset; ?>" sizes="(max-width: 479px) 200px, (max-width: 767px) 35vw, 200px" alt="<?php echo $udesly_fe_items['image_-a5ed80f']->alt; ?>" data-udy-fe="image_-a5ed80f"></div>
    <div class="text-cont _3">
      <h1 class="h1-latin-cravings" data-udy-fe="text_2efa875a,text_2f3ee476"><?php echo $udesly_fe_items['text_2efa875a'] ?><br><?php echo $udesly_fe_items['text_2f3ee476'] ?></h1>
      <h2 class="h2-latin-cravings" data-udy-fe="text_3a99d6c3"><?php echo $udesly_fe_items['text_3a99d6c3'] ?></h2><a href="<?php echo $udesly_fe_items['link_739335b8']; ?>" class="latin-cravings-btn w-button" data-udy-fe="text_-5fa13623,link_739335b8"><?php echo $udesly_fe_items['text_-5fa13623'] ?></a></div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-592817c6']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-592817c6']->alt; ?>" class="hero-avatar" data-udy-fe="image_-592817c6" srcset="<?php echo $udesly_fe_items['image_-592817c6']->srcset; ?>">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_-48a69fa4"><?php echo $udesly_fe_items['text_-48a69fa4'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_-29b98c5"><?php echo $udesly_fe_items['text_-29b98c5'] ?><br></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_5988be8b"><?php echo $udesly_fe_items['text_5988be8b'] ?></h6><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social facebook w-inline-block" data-udy-fe="link_23"></a><a href="<?php echo $udesly_fe_items['link_45e3c6f2']; ?>" class="hero-social insta w-inline-block" data-udy-fe="link_45e3c6f2"></a><a href="<?php echo $udesly_fe_items['link_739335b8']; ?>" class="hero-social whatsapp w-inline-block" data-udy-fe="link_739335b8"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_645efc51"><?php echo $udesly_fe_items['text_645efc51'] ?></h1>
        <p class="p-footer" data-udy-fe="text_553e063d"><?php echo $udesly_fe_items['text_553e063d'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-507c1747"><?php echo $udesly_fe_items['text_-507c1747'] ?></h1>
        <p class="p-footer" data-udy-fe="text_-3ae39ac3"><?php echo $udesly_fe_items['text_-3ae39ac3'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590918372690" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <style data-udy-fe="text_20e2f9b2"><?php echo $udesly_fe_items['text_20e2f9b2'] ?></style>

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'latin-cravings'); ?></body></html>