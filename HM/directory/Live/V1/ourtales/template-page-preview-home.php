<!DOCTYPE html>
<?php /*
        Template Name: preview-home
        */ ?> 
        <html data-wf-page="5ec12dbcacbc5647aa0a9022" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590918372690" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590918372690" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590918372690" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.gif?v=1590918372690" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.jpg?v=1590918372690" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('preview-home'); ?></head>
<body class="<?php echo join(' ', get_body_class() ) . ' body'; ?>" udesly-page="preview-home"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar w-nav">
    <div class="navbar-cont w-container"><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="brand w-nav-brand" data-udy-fe="link_23"><img src="<?php echo $udesly_fe_items['image_4eed19bd']->src; ?>" width="125" alt="<?php echo $udesly_fe_items['image_4eed19bd']->alt; ?>" class="ourtales-logo" data-udy-fe="image_4eed19bd" srcset="<?php echo $udesly_fe_items['image_4eed19bd']->srcset; ?>"></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div page="template" class="welcome home">
    <div class="w-container">
      <div class="center-mini-section">
        <h1 class="heading home" data-udy-fe="text_-4514cf30"><?php echo $udesly_fe_items['text_-4514cf30'] ?></h1>
        <p class="p-home" data-udy-fe="text_-4235dd7b,text_200d,text_-496bb0d1"><?php echo $udesly_fe_items['text_-4235dd7b'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br><?php echo $udesly_fe_items['text_-496bb0d1'] ?><br></p>
        <p class="p-home _2" data-udy-fe="text_-66b122ac"><?php echo $udesly_fe_items['text_-66b122ac'] ?></p>
      </div>
    </div>
  </div>
  <div class="tales stores">
    <h2 class="heading h2-home" data-udy-fe="text_7628ee00"><?php echo $udesly_fe_items['text_7628ee00'] ?></h2>
    <div class="tale-scroll-track">
      <div class="tale-container store-wrapper">
        <div class="tale-track store">
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_-7ad3c64']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_-7ad3c64">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_3685a2c3']->src; ?>" alt="<?php echo $udesly_fe_items['image_3685a2c3']->alt; ?>" class="img-store _1" data-udy-fe="image_3685a2c3" srcset="<?php echo $udesly_fe_items['image_3685a2c3']->srcset; ?>"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_-462632b0"><?php echo $udesly_fe_items['text_-462632b0'] ?></h2>
          </div>
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_5f0b3a23']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_5f0b3a23">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_-58d6264e']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-58d6264e']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_-58d6264e']->alt; ?>" class="img-store _3" data-udy-fe="image_-58d6264e"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_4cdd6f7a"><?php echo $udesly_fe_items['text_4cdd6f7a'] ?></h2>
          </div>
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_-2ac9cdd5']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_-2ac9cdd5">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_-4a0fb876']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-4a0fb876']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_-4a0fb876']->alt; ?>" class="img-store _4" data-udy-fe="image_-4a0fb876"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_-5b8644e5"><?php echo $udesly_fe_items['text_-5b8644e5'] ?></h2>
          </div>
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_-7d4407ba']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_-7d4407ba">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_c3583bd']->src; ?>" srcset="<?php echo $udesly_fe_items['image_c3583bd']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_c3583bd']->alt; ?>" class="img-store _2" data-udy-fe="image_c3583bd"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_112d9c44"><?php echo $udesly_fe_items['text_112d9c44'] ?></h2>
          </div>
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_-3a3b7ecb']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_-3a3b7ecb">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_-31c910c0']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-31c910c0']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_-31c910c0']->alt; ?>" class="img-store _4" data-udy-fe="image_-31c910c0"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_-a7b522f"><?php echo $udesly_fe_items['text_-a7b522f'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tales stores">
    <h2 class="heading h2-home" data-udy-fe="text_765cfc9c"><?php echo $udesly_fe_items['text_765cfc9c'] ?></h2>
    <div class="tale-scroll-track">
      <div class="tale-container store-wrapper">
        <div class="tale-track store">
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_-722e0161']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_-722e0161">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_53e6eb5e']->src; ?>" alt="<?php echo $udesly_fe_items['image_53e6eb5e']->alt; ?>" class="img-store _7" data-udy-fe="image_53e6eb5e" srcset="<?php echo $udesly_fe_items['image_53e6eb5e']->srcset; ?>"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_38b54dc"><?php echo $udesly_fe_items['text_38b54dc'] ?></h2>
          </div>
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_7f2580c3']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_7f2580c3">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_-420204e']->src; ?>" alt="<?php echo $udesly_fe_items['image_-420204e']->alt; ?>" class="img-store _7" data-udy-fe="image_-420204e" srcset="<?php echo $udesly_fe_items['image_-420204e']->srcset; ?>"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_-5ba47f21"><?php echo $udesly_fe_items['text_-5ba47f21'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tales stores">
    <h2 class="heading h2-home" data-udy-fe="text_79ceadde"><?php echo $udesly_fe_items['text_79ceadde'] ?></h2>
    <div class="tale-scroll-track">
      <div class="tale-container store-wrapper">
        <div class="tale-track store">
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_79b68272']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_79b68272">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_553fb51']->src; ?>" srcset="<?php echo $udesly_fe_items['image_553fb51']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_553fb51']->alt; ?>" class="img-store _6" data-udy-fe="image_553fb51"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_7f8e1801"><?php echo $udesly_fe_items['text_7f8e1801'] ?></h2>
          </div>
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_6b772119']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_6b772119">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_5005848a']->src; ?>" alt="<?php echo $udesly_fe_items['image_5005848a']->alt; ?>" class="img-store _6" data-udy-fe="image_5005848a" srcset="<?php echo $udesly_fe_items['image_5005848a']->srcset; ?>"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_345f400"><?php echo $udesly_fe_items['text_345f400'] ?></h2>
          </div>
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_12288238']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_12288238">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_3a6c5b9d']->src; ?>" srcset="<?php echo $udesly_fe_items['image_3a6c5b9d']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_3a6c5b9d']->alt; ?>" class="img-store _6" data-udy-fe="image_3a6c5b9d"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_-29e83785"><?php echo $udesly_fe_items['text_-29e83785'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div page="template" class="welcome home">
    <div class="w-container">
      <div class="center-mini-section">
        <h1 class="heading home" data-udy-fe="text_-4b0f148d"><?php echo $udesly_fe_items['text_-4b0f148d'] ?></h1>
        <p class="h2-hero black" data-udy-fe="text_65e738f4"><?php echo $udesly_fe_items['text_65e738f4'] ?><br></p>
        <div class="coming-soon-cont"><img src="<?php echo $udesly_fe_items['image_-6c9290ea']->src; ?>" width="90" alt="<?php echo $udesly_fe_items['image_-6c9290ea']->alt; ?>" class="img-coming-soon" data-udy-fe="image_-6c9290ea" srcset="<?php echo $udesly_fe_items['image_-6c9290ea']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_3a10c287']->src; ?>" width="90" alt="<?php echo $udesly_fe_items['image_3a10c287']->alt; ?>" class="img-coming-soon" data-udy-fe="image_3a10c287" srcset="<?php echo $udesly_fe_items['image_3a10c287']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_33cf3ad1']->src; ?>" width="90" alt="<?php echo $udesly_fe_items['image_33cf3ad1']->alt; ?>" class="img-coming-soon" data-udy-fe="image_33cf3ad1" srcset="<?php echo $udesly_fe_items['image_33cf3ad1']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_-3516d3d0']->src; ?>" width="90" alt="<?php echo $udesly_fe_items['image_-3516d3d0']->alt; ?>" class="img-coming-soon" data-udy-fe="image_-3516d3d0" srcset="<?php echo $udesly_fe_items['image_-3516d3d0']->srcset; ?>"></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_645efc51"><?php echo $udesly_fe_items['text_645efc51'] ?></h1>
        <p class="p-footer" data-udy-fe="text_553e063d"><?php echo $udesly_fe_items['text_553e063d'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-507c1747"><?php echo $udesly_fe_items['text_-507c1747'] ?></h1>
        <p class="p-footer" data-udy-fe="text_-3ae39ac3"><?php echo $udesly_fe_items['text_-3ae39ac3'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590918372690" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'preview-home'); ?></body></html>