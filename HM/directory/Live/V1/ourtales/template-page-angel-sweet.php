<!DOCTYPE html>
<?php /*
        Template Name: angel-sweet
        */ ?> 
        <html data-wf-page="5ed075e214cfa8b1f2b4887c" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  
  
  <meta content="Angel Sweet" property="twitter:title">
  <meta content="Birthday Cake / Chocolate Cake / Caramel Cake / 3 Milks Cake / Vanilla Cake / Orange Cake / Lactose-free Milk Cake / Roasted Milk Dessert / Chocolate Mousse / Flan Quesillo / Tiramisu / Cheesecake" property="twitter:description">
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590918372690" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590918372690" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590918372690" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.gif?v=1590918372690" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.jpg?v=1590918372690" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('angel-sweet'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="angel-sweet"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_4eed19bd']->src; ?>" width="125" alt="<?php echo $udesly_fe_items['image_4eed19bd']->alt; ?>" class="ourtales-logo" data-udy-fe="image_4eed19bd" srcset="<?php echo $udesly_fe_items['image_4eed19bd']->srcset; ?>"></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="tales-basic">
    <div data-animation="slide" data-duration="500" data-infinite="1" class="tales-basic-slider w-slider">
      <div class="w-slider-mask">
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_5805b138']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5805b138']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_5805b138']->alt; ?>" class="hero-slide-img" data-udy-fe="image_5805b138"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_57e98236']->src; ?>" srcset="<?php echo $udesly_fe_items['image_57e98236']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_57e98236']->alt; ?>" class="hero-slide-img" data-udy-fe="image_57e98236"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper buttom"><img src="<?php echo $udesly_fe_items['image_582ff7bb']->src; ?>" srcset="<?php echo $udesly_fe_items['image_582ff7bb']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_582ff7bb']->alt; ?>" class="hero-slide-img" data-udy-fe="image_582ff7bb"></div>
        </div>
      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav w-round"></div>
    </div>
  </div>
  <div class="intro">
    <div class="page-container w-container">
      <div class="logo-cont"><img src="<?php echo $udesly_fe_items['image_23bafdde']->src; ?>" width="200" srcset="<?php echo $udesly_fe_items['image_23bafdde']->srcset; ?>" sizes="200px" alt="<?php echo $udesly_fe_items['image_23bafdde']->alt; ?>" class="round-logo shadow" data-udy-fe="image_23bafdde"></div>
      <div class="text-cont">
        <h1 class="h1-angel-sweet" data-udy-fe="text_55353e47"><?php echo $udesly_fe_items['text_55353e47'] ?></h1>
        <h2 class="h2-angel-sweet" data-udy-fe="text_-6e423a1b"><?php echo $udesly_fe_items['text_-6e423a1b'] ?></h2>
        <p data-udy-fe="text_8a95659,text_3b6646f1"><?php echo $udesly_fe_items['text_8a95659'] ?><br><?php echo $udesly_fe_items['text_3b6646f1'] ?></p>
      </div>
    </div>
  </div>
  <div class="products angel-sweet">
    <div class="page-container _3 w-container">
      <h1 class="h1-angel-sweet _2" data-udy-fe="text_-6febaea8"><?php echo $udesly_fe_items['text_-6febaea8'] ?></h1>
      <div class="product-wrapper">
        <div id="w-node-50d50a3bd31d-f2b4887c" class="product-desc">
          <h1 class="h2-angel-sweet _2" data-udy-fe="text_3dddd1b"><?php echo $udesly_fe_items['text_3dddd1b'] ?></h1>
          <ul role="list" class="w-list-unstyled">
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_-34f1ac7d']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_-34f1ac7d']->alt; ?>" data-udy-fe="image_-34f1ac7d" srcset="<?php echo $udesly_fe_items['image_-34f1ac7d']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-4c230dd5"><?php echo $udesly_fe_items['text_-4c230dd5'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_-34f1ac7d']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_-34f1ac7d']->alt; ?>" data-udy-fe="image_-34f1ac7d" srcset="<?php echo $udesly_fe_items['image_-34f1ac7d']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-5f8a7de4"><?php echo $udesly_fe_items['text_-5f8a7de4'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_-34f1ac7d']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_-34f1ac7d']->alt; ?>" data-udy-fe="image_-34f1ac7d" srcset="<?php echo $udesly_fe_items['image_-34f1ac7d']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-7bc43479"><?php echo $udesly_fe_items['text_-7bc43479'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_-34f1ac7d']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_-34f1ac7d']->alt; ?>" data-udy-fe="image_-34f1ac7d" srcset="<?php echo $udesly_fe_items['image_-34f1ac7d']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-3b18ae95"><?php echo $udesly_fe_items['text_-3b18ae95'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_-34f1ac7d']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_-34f1ac7d']->alt; ?>" data-udy-fe="image_-34f1ac7d" srcset="<?php echo $udesly_fe_items['image_-34f1ac7d']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_711d735b"><?php echo $udesly_fe_items['text_711d735b'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_-34f1ac7d']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_-34f1ac7d']->alt; ?>" data-udy-fe="image_-34f1ac7d" srcset="<?php echo $udesly_fe_items['image_-34f1ac7d']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-72bcedb2"><?php echo $udesly_fe_items['text_-72bcedb2'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_-34f1ac7d']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_-34f1ac7d']->alt; ?>" data-udy-fe="image_-34f1ac7d" srcset="<?php echo $udesly_fe_items['image_-34f1ac7d']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-282deb62"><?php echo $udesly_fe_items['text_-282deb62'] ?><br></p>
            </li>
          </ul>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd326-f2b4887c" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5c6d486d']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5c6d486d']->srcset; ?>" sizes="(max-width: 479px) 100vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_5c6d486d']->alt; ?>" class="img-product" data-udy-fe="image_5c6d486d"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5821e03a']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5821e03a']->srcset; ?>" sizes="(max-width: 479px) 100vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_5821e03a']->alt; ?>" class="img-product" data-udy-fe="image_5821e03a"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-50d50a3bd334-f2b4887c" class="product-desc">
          <h1 class="h2-angel-sweet _2" data-udy-fe="text_4166ce2d"><?php echo $udesly_fe_items['text_4166ce2d'] ?></h1>
          <ul role="list" class="w-list-unstyled">
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_3b8c0ca8']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_3b8c0ca8']->alt; ?>" data-udy-fe="image_3b8c0ca8" srcset="<?php echo $udesly_fe_items['image_3b8c0ca8']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-31f3ef89"><?php echo $udesly_fe_items['text_-31f3ef89'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_3b8c0ca8']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_3b8c0ca8']->alt; ?>" data-udy-fe="image_3b8c0ca8" srcset="<?php echo $udesly_fe_items['image_3b8c0ca8']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-422b2b4a"><?php echo $udesly_fe_items['text_-422b2b4a'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_3b8c0ca8']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_3b8c0ca8']->alt; ?>" data-udy-fe="image_3b8c0ca8" srcset="<?php echo $udesly_fe_items['image_3b8c0ca8']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_1fe16cc5"><?php echo $udesly_fe_items['text_1fe16cc5'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_3b8c0ca8']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_3b8c0ca8']->alt; ?>" data-udy-fe="image_3b8c0ca8" srcset="<?php echo $udesly_fe_items['image_3b8c0ca8']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-6f9c757e"><?php echo $udesly_fe_items['text_-6f9c757e'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_3b8c0ca8']->src; ?>" width="20" alt="<?php echo $udesly_fe_items['image_3b8c0ca8']->alt; ?>" data-udy-fe="image_3b8c0ca8" srcset="<?php echo $udesly_fe_items['image_3b8c0ca8']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-30532851"><?php echo $udesly_fe_items['text_-30532851'] ?><br></p>
            </li>
          </ul>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd355-f2b4887c" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_57e98236']->src; ?>" srcset="<?php echo $udesly_fe_items['image_57e98236']->srcset; ?>" sizes="(max-width: 479px) 100vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_57e98236']->alt; ?>" class="img-product" data-udy-fe="image_57e98236"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_583e0f3c']->src; ?>" srcset="<?php echo $udesly_fe_items['image_583e0f3c']->srcset; ?>" sizes="(max-width: 479px) 100vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_583e0f3c']->alt; ?>" class="img-product" data-udy-fe="image_583e0f3c"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="ico-wrapper bigger"><img src="<?php echo $udesly_fe_items['image_23bafdde']->src; ?>" srcset="<?php echo $udesly_fe_items['image_23bafdde']->srcset; ?>" sizes="100vw" alt="<?php echo $udesly_fe_items['image_23bafdde']->alt; ?>" data-udy-fe="image_23bafdde"></div>
    <div class="text-cont _3">
      <h1 class="h1-angel-sweet" data-udy-fe="text_202f7f41,text_-11f22874"><?php echo $udesly_fe_items['text_202f7f41'] ?><br><?php echo $udesly_fe_items['text_-11f22874'] ?><br></h1>
      <h2 class="h2-angel-sweet" data-udy-fe="text_-5b71ea1d,text_-5e01e66"><?php echo $udesly_fe_items['text_-5b71ea1d'] ?><br><?php echo $udesly_fe_items['text_-5e01e66'] ?></h2>
      <p data-udy-fe="text_-2c7fdb03"><?php echo $udesly_fe_items['text_-2c7fdb03'] ?><br></p><a href="<?php echo $udesly_fe_items['link_6a2c65ed']; ?>" class="angel-sweet-btn w-button" data-udy-fe="text_-5fa13623,link_6a2c65ed"><?php echo $udesly_fe_items['text_-5fa13623'] ?></a></div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-592817c6']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-592817c6']->alt; ?>" class="hero-avatar" data-udy-fe="image_-592817c6" srcset="<?php echo $udesly_fe_items['image_-592817c6']->srcset; ?>">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_7d0f918"><?php echo $udesly_fe_items['text_7d0f918'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_ae997f4"><?php echo $udesly_fe_items['text_ae997f4'] ?></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_2fc6beb0"><?php echo $udesly_fe_items['text_2fc6beb0'] ?></h6><a href="<?php echo $udesly_fe_items['link_63bcfa8c']; ?>" class="hero-social facebook w-inline-block" data-udy-fe="link_63bcfa8c"></a><a href="<?php echo $udesly_fe_items['link_-a069194']; ?>" class="hero-social insta w-inline-block" data-udy-fe="link_-a069194"></a><a href="<?php echo $udesly_fe_items['link_6a2c65ed']; ?>" class="hero-social whatsapp w-inline-block" data-udy-fe="link_6a2c65ed"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_645efc51"><?php echo $udesly_fe_items['text_645efc51'] ?></h1>
        <p class="p-footer" data-udy-fe="text_553e063d"><?php echo $udesly_fe_items['text_553e063d'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-507c1747"><?php echo $udesly_fe_items['text_-507c1747'] ?></h1>
        <p class="p-footer" data-udy-fe="text_-3ae39ac3"><?php echo $udesly_fe_items['text_-3ae39ac3'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590918372690" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <style data-udy-fe="text_-5fd4c281"><?php echo $udesly_fe_items['text_-5fd4c281'] ?></style>

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'angel-sweet'); ?></body></html>