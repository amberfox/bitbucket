<!DOCTYPE html>
<html data-wf-page="5ec12dbcacbc5647aa0a9022" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590060366015" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590060366015" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590060366015" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Roboto:100,300,regular,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590060366015" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590060366015" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('preview-home'); ?></head>
<body class="<?php echo join(' ', get_body_class() ) . ' body'; ?>" udesly-page="preview-home"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar w-nav">
    <div class="navbar-cont w-container">
      <a href="<?php echo $udesly_fe_items['link_23']; ?>" class="brand w-nav-brand" data-udy-fe="link_23">
        <div class="our-logo" data-udy-fe="text_33ce569f"><?php echo $udesly_fe_items['text_33ce569f'] ?></div>
      </a>
      <nav role="navigation" class="nav-menu w-nav-menu"><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="nav-link-2 w-nav-link" data-udy-fe="text_3c2336d,link_23"><?php echo $udesly_fe_items['text_3c2336d'] ?></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="nav-link-2 w-nav-link" data-udy-fe="text_-64104400,link_23"><?php echo $udesly_fe_items['text_-64104400'] ?></a></nav>
      <div class="w-nav-button">
        <div class="w-icon-nav-menu"></div>
      </div>
    </div>
  </div>
  <div page="template" class="welcome">
    <div class="w-container">
      <div class="center-mini-section">
        <h1 class="heading" data-udy-fe="text_609b7574"><?php echo $udesly_fe_items['text_609b7574'] ?></h1>
        <p data-udy-fe="text_-23b19f02"><?php echo $udesly_fe_items['text_-23b19f02'] ?></p>
      </div>
    </div>
  </div>
  <div class="tales stores">
    <div class="tale-scroll-track">
      <div class="tale-container store-wrapper">
        <div class="tale-track store">
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_-7ad3c64']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_-7ad3c64">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_3685a2c3']->src; ?>" alt="<?php echo $udesly_fe_items['image_3685a2c3']->alt; ?>" class="img-store _1" data-udy-fe="image_3685a2c3" srcset="<?php echo $udesly_fe_items['image_3685a2c3']->srcset; ?>"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_-462632b0"><?php echo $udesly_fe_items['text_-462632b0'] ?></h2>
          </div>
          <div class="store-container">
            <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_-216f13e4']->src; ?>" alt="<?php echo $udesly_fe_items['image_-216f13e4']->alt; ?>" class="img-store _2" data-udy-fe="image_-216f13e4" srcset="<?php echo $udesly_fe_items['image_-216f13e4']->srcset; ?>"></div>
            <h2 class="h2-store-name" data-udy-fe="text_-36ee401b"><?php echo $udesly_fe_items['text_-36ee401b'] ?></h2>
          </div>
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_18bc0f2a']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_18bc0f2a">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_-1f38fb23']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-1f38fb23']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_-1f38fb23']->alt; ?>" class="img-store _3" data-udy-fe="image_-1f38fb23"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_621bf717"><?php echo $udesly_fe_items['text_621bf717'] ?></h2>
          </div>
          <div class="store-container">
            <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_-4a0fb876']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-4a0fb876']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_-4a0fb876']->alt; ?>" class="img-store _4" data-udy-fe="image_-4a0fb876"></div>
            <h2 class="h2-store-name" data-udy-fe="text_-5b8644e5"><?php echo $udesly_fe_items['text_-5b8644e5'] ?></h2>
          </div>
          <div class="store-container">
            <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_37c2e1dd']->src; ?>" srcset="<?php echo $udesly_fe_items['image_37c2e1dd']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_37c2e1dd']->alt; ?>" class="img-store _5" data-udy-fe="image_37c2e1dd"></div>
            <h2 class="h2-store-name" data-udy-fe="text_-56b430ad"><?php echo $udesly_fe_items['text_-56b430ad'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tales stores">
    <div class="tale-scroll-track">
      <div class="tale-container store-wrapper">
        <div class="tale-track store">
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_23']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_23">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_2bc4c53e']->src; ?>" alt="<?php echo $udesly_fe_items['image_2bc4c53e']->alt; ?>" class="img-store _6" data-udy-fe="image_2bc4c53e" srcset="<?php echo $udesly_fe_items['image_2bc4c53e']->srcset; ?>"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_-7560ef31"><?php echo $udesly_fe_items['text_-7560ef31'] ?></h2>
          </div>
          <div class="store-container">
            <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_53e6eb5e']->src; ?>" alt="<?php echo $udesly_fe_items['image_53e6eb5e']->alt; ?>" class="img-store _7" data-udy-fe="image_53e6eb5e" srcset="<?php echo $udesly_fe_items['image_53e6eb5e']->srcset; ?>"></div>
            <h2 class="h2-store-name" data-udy-fe="text_38b54dc"><?php echo $udesly_fe_items['text_38b54dc'] ?></h2>
          </div>
          <div class="store-container">
            <a href="<?php echo $udesly_fe_items['link_18bc0f2a']; ?>" class="thumb-link w-inline-block" data-udy-fe="link_18bc0f2a">
              <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_-1f38fb23']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-1f38fb23']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_-1f38fb23']->alt; ?>" class="img-store _3" data-udy-fe="image_-1f38fb23"></div>
            </a>
            <h2 class="h2-store-name" data-udy-fe="text_621bf717"><?php echo $udesly_fe_items['text_621bf717'] ?></h2>
          </div>
          <div class="store-container">
            <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_-4a0fb876']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-4a0fb876']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_-4a0fb876']->alt; ?>" class="img-store _4" data-udy-fe="image_-4a0fb876"></div>
            <h2 class="h2-store-name" data-udy-fe="text_-5b8644e5"><?php echo $udesly_fe_items['text_-5b8644e5'] ?></h2>
          </div>
          <div class="store-container">
            <div class="store-thumb"><img src="<?php echo $udesly_fe_items['image_37c2e1dd']->src; ?>" srcset="<?php echo $udesly_fe_items['image_37c2e1dd']->srcset; ?>" sizes="(max-width: 479px) 56vw, (max-width: 767px) 52vw, (max-width: 991px) 34vw, 300px" alt="<?php echo $udesly_fe_items['image_37c2e1dd']->alt; ?>" class="img-store" data-udy-fe="image_37c2e1dd"></div>
            <h2 class="h2-store-name" data-udy-fe="text_-56b430ad"><?php echo $udesly_fe_items['text_-56b430ad'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590060366015" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'preview-home'); ?></body></html>