<!DOCTYPE html>
<?php /*
        Template Name: sweet-bakery
        */ ?> 
        <html data-wf-page="5ec49673db25b4405158210f" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  <meta content="Sweet Bakery" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590114981664" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590114981664" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590114981664" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Roboto:100,300,regular,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590114981664" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590114981664" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('sweet-bakery'); ?></head>
<body class="<?php echo join(' ', get_body_class() ) . ' body'; ?>" udesly-page="sweet-bakery"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_-5b12c530']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_-5b12c530']->alt; ?>" class="arrow-icon" data-udy-fe="image_-5b12c530" srcset="<?php echo $udesly_fe_items['image_-5b12c530']->srcset; ?>"><div class="our-logo" data-udy-fe="text_33ce569f"><?php echo $udesly_fe_items['text_33ce569f'] ?></div></a></div>
  </div>
  <div class="tales">
    <div class="tale-scroll-track">
      <div class="tale-container">
        <div class="tale-track page">
          <div class="scene-container">
            <div id="w-node-9a66a06ac283-5158210f" class="service-name _1">
              <h1 class="h2-hero" data-udy-fe="text_313ced66,text_651874e"><?php echo $udesly_fe_items['text_313ced66'] ?><br><?php echo $udesly_fe_items['text_651874e'] ?></h1>
              <h1 class="h1-hero" data-udy-fe="text_68c2ef0,text_-533bfe20"><?php echo $udesly_fe_items['text_68c2ef0'] ?><br><?php echo $udesly_fe_items['text_-533bfe20'] ?></h1>
            </div>
          </div>
          <div class="scene-container _2"></div>
          <div class="scene-container _3"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="intro">
    <div class="sweet-container w-container">
      <div class="logo-cont"><img src="<?php echo $udesly_fe_items['image_29883ba3']->src; ?>" srcset="<?php echo $udesly_fe_items['image_29883ba3']->srcset; ?>" sizes="200px" alt="<?php echo $udesly_fe_items['image_29883ba3']->alt; ?>" data-udy-fe="image_29883ba3"></div>
      <div class="text-cont">
        <h1 class="h1-sweet-bakery" data-udy-fe="text_-64aade83"><?php echo $udesly_fe_items['text_-64aade83'] ?></h1>
        <h2 class="h2-sweet" data-udy-fe="text_-3c6fb500"><?php echo $udesly_fe_items['text_-3c6fb500'] ?></h2>
        <p data-w-id="275b2901-447d-2417-12dc-53847ca06cd3" data-udy-fe="text_-2985f882"><?php echo $udesly_fe_items['text_-2985f882'] ?><br></p>
      </div>
    </div>
  </div>
  <div class="what">
    <div class="sweet-container _2 w-container">
      <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_-3a4de922']->src; ?>" width="131" srcset="<?php echo $udesly_fe_items['image_-3a4de922']->srcset; ?>" sizes="131px" alt="<?php echo $udesly_fe_items['image_-3a4de922']->alt; ?>" class="ico-sweet-bakery _2" data-udy-fe="image_-3a4de922"></div>
      <div class="text-cont _2">
        <h1 class="h2-sweet" data-udy-fe="text_6dde0d3a"><?php echo $udesly_fe_items['text_6dde0d3a'] ?></h1>
        <p data-udy-fe="text_-5904c839,text_103a7d,text_7d2a4b2,text_3ad615f1,text_200d,text_796c534d"><?php echo $udesly_fe_items['text_-5904c839'] ?><strong data-udy-fe="text_-21c95a28"><?php echo $udesly_fe_items['text_-21c95a28'] ?></strong><?php echo $udesly_fe_items['text_103a7d'] ?><strong data-udy-fe="text_-4f3131bc"><?php echo $udesly_fe_items['text_-4f3131bc'] ?></strong><?php echo $udesly_fe_items['text_7d2a4b2'] ?><strong data-udy-fe="text_-7741696b"><?php echo $udesly_fe_items['text_-7741696b'] ?></strong><?php echo $udesly_fe_items['text_3ad615f1'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br><?php echo $udesly_fe_items['text_796c534d'] ?><br></p>
      </div>
    </div>
  </div>
  <div class="what">
    <div class="sweet-container w-container">
      <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_-3a5c00a3']->src; ?>" width="131" srcset="<?php echo $udesly_fe_items['image_-3a5c00a3']->srcset; ?>" sizes="131px" alt="<?php echo $udesly_fe_items['image_-3a5c00a3']->alt; ?>" class="ico-sweet-bakery" data-udy-fe="image_-3a5c00a3"></div>
      <div class="text-cont _2">
        <h1 class="h2-sweet" data-udy-fe="text_-14f04171"><?php echo $udesly_fe_items['text_-14f04171'] ?></h1>
        <p data-udy-fe="text_-18f96515"><?php echo $udesly_fe_items['text_-18f96515'] ?><br></p>
      </div>
    </div>
  </div>
  <div class="prices">
    <div class="sweet-container _3 w-container">
      <h1 class="h1-sweet-bakery _2" data-udy-fe="text_-bd8dbce"><?php echo $udesly_fe_items['text_-bd8dbce'] ?></h1>
      <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_-3a3fd1a1']->src; ?>" width="221" srcset="<?php echo $udesly_fe_items['image_-3a3fd1a1']->srcset; ?>" sizes="200px" alt="<?php echo $udesly_fe_items['image_-3a3fd1a1']->alt; ?>" class="ico-sweet-bakery _3" data-udy-fe="image_-3a3fd1a1"></div>
      <div class="product-wrapper">
        <div id="w-node-e3c3d624ce9d-5158210f" class="product-desc">
          <h2 class="h2-sweet" data-udy-fe="text_42bb235c"><?php echo $udesly_fe_items['text_42bb235c'] ?></h2>
          <p data-udy-fe="text_-7fae5c8"><?php echo $udesly_fe_items['text_-7fae5c8'] ?><br></p>
          <p class="price" data-udy-fe="text_49cc129"><?php echo $udesly_fe_items['text_49cc129'] ?><br></p>
        </div>
        <a href="<?php echo $udesly_fe_items['link_23']; ?>" id="w-node-dd7a074e676d-5158210f" class="light-product w-inline-block w-lightbox" data-udy-fe="link_23">
          <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-69614d54']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-69614d54']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 84vw, (max-width: 991px) 434.65625px, 576px" alt="<?php echo $udesly_fe_items['image_-69614d54']->alt; ?>" class="img-product" data-udy-fe="image_-69614d54"></div>
          <script type="application/json" class="w-json">{"items":[{"width":1080,"caption":"Brazo de Reina / Sponge Roll","height":1080,"fileName":"5ec4ac6d86f0e487b91b81e9_sweet-bakery-2.jpg","origFileName":"sweet-bakery-2.jpg","url":"<?php echo get_stylesheet_directory_uri(); ?>/images/sweet-bakery-2.jpg?v=1590114981664","_id":"5ec4ac6d86f0e487b91b81e9","type":"image","fileSize":77111}],"group":"products"}</script>
        </a>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-a8e7363fe674-5158210f" class="product-desc">
          <h2 class="h2-sweet" data-udy-fe="text_42bb235c"><?php echo $udesly_fe_items['text_42bb235c'] ?></h2>
          <p data-udy-fe="text_-7fae5c8"><?php echo $udesly_fe_items['text_-7fae5c8'] ?><br></p>
          <p class="price" data-udy-fe="text_49cc129"><?php echo $udesly_fe_items['text_49cc129'] ?><br></p>
        </div>
        <a href="<?php echo $udesly_fe_items['link_23']; ?>" id="w-node-a8e7363fe67d-5158210f" class="light-product w-inline-block w-lightbox" data-udy-fe="link_23">
          <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-695335d3']->src; ?>" alt="<?php echo $udesly_fe_items['image_-695335d3']->alt; ?>" class="img-product _2" data-udy-fe="image_-695335d3" srcset="<?php echo $udesly_fe_items['image_-695335d3']->srcset; ?>"></div>
          <script type="application/json" class="w-json">{"items":[{"type":"image","_id":"5ec4acb2b66aa0cd4256cadf","fileName":"5ec4acb2b66aa0cd4256cadf_sweet-bakery-3.jpg","origFileName":"sweet-bakery-3.jpg","width":916,"height":916,"fileSize":63106,"url":"<?php echo get_stylesheet_directory_uri(); ?>/images/sweet-bakery-3.jpg?v=1590114981664"}],"group":"products"}</script>
        </a>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_-3a31ba20']->src; ?>" width="200" srcset="<?php echo $udesly_fe_items['image_-3a31ba20']->srcset; ?>" sizes="(max-width: 479px) 200px, (max-width: 767px) 30vw, 200px" alt="<?php echo $udesly_fe_items['image_-3a31ba20']->alt; ?>" class="ico-sweet-bakery _4" data-udy-fe="image_-3a31ba20"></div>
    <div class="text-cont _3">
      <h1 class="h1-sweet-bakery _3" data-udy-fe="text_-8645c2c"><?php echo $udesly_fe_items['text_-8645c2c'] ?></h1>
      <h2 class="h2-sweet" data-udy-fe="text_-3c304cdb"><?php echo $udesly_fe_items['text_-3c304cdb'] ?></h2><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="sweet-bakery-btn w-button" data-udy-fe="text_-420a83c8,link_23"><?php echo $udesly_fe_items['text_-420a83c8'] ?></a></div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-267663f5']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-267663f5']->alt; ?>" class="hero-avatar" data-udy-fe="image_-267663f5" srcset="<?php echo $udesly_fe_items['image_-267663f5']->srcset; ?>">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_-70ecd892"><?php echo $udesly_fe_items['text_-70ecd892'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_3f1ae3e3"><?php echo $udesly_fe_items['text_3f1ae3e3'] ?><br></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_42cccd7d"><?php echo $udesly_fe_items['text_42cccd7d'] ?></h6><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social w-inline-block" data-udy-fe="link_23"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social insta w-inline-block" data-udy-fe="link_23"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social whatsapp w-inline-block" data-udy-fe="link_23"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590114981664" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'sweet-bakery'); ?></body></html>