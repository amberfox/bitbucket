<!DOCTYPE html>
<?php /*
        Template Name: custom-mug-style
        */ ?> 
        <html data-wf-page="5eceff5e9d8fbe5c9add9b8f" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  
  
  <meta content="Custom Mug Style" property="twitter:title">
  <meta content="Custom Mugs / Bottles / Glass / Apparel &amp; more." property="twitter:description">
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590665080848" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590665080848" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590665080848" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590665080848" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590665080848" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('custom-mug-style'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="custom-mug-style"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_-5b12c530']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_-5b12c530']->alt; ?>" class="arrow-icon" data-udy-fe="image_-5b12c530" srcset="<?php echo $udesly_fe_items['image_-5b12c530']->srcset; ?>"><div class="our-logo" data-udy-fe="text_6bfa9e79"><?php echo $udesly_fe_items['text_6bfa9e79'] ?></div></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="tales-basic">
    <div data-animation="slide" data-duration="500" data-infinite="1" class="tales-basic-slider full-width w-slider">
      <div class="w-slider-mask">
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper full-width"><img src="<?php echo $udesly_fe_items['image_-735aed1a']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-735aed1a']->srcset; ?>" sizes="100vw" alt="<?php echo $udesly_fe_items['image_-735aed1a']->alt; ?>" class="hero-slide-img" data-udy-fe="image_-735aed1a"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper full-width"><img src="<?php echo $udesly_fe_items['image_-71a6147b']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-71a6147b']->srcset; ?>" sizes="100vw" alt="<?php echo $udesly_fe_items['image_-71a6147b']->alt; ?>" class="hero-slide-img" data-udy-fe="image_-71a6147b"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper full-width"><img src="<?php echo $udesly_fe_items['image_-6ff13bdc']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-6ff13bdc']->srcset; ?>" sizes="100vw" alt="<?php echo $udesly_fe_items['image_-6ff13bdc']->alt; ?>" class="hero-slide-img" data-udy-fe="image_-6ff13bdc"></div>
        </div>
      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav w-round"></div>
    </div>
  </div>
  <div class="intro">
    <div class="page-container w-container">
      <div class="logo-cont"><img src="<?php echo $udesly_fe_items['image_-6e0d1373']->src; ?>" width="200" alt="<?php echo $udesly_fe_items['image_-6e0d1373']->alt; ?>" class="round-logo" data-udy-fe="image_-6e0d1373" srcset="<?php echo $udesly_fe_items['image_-6e0d1373']->srcset; ?>"></div>
      <div class="text-cont">
        <h1 class="h1-custom-mug-style" data-udy-fe="text_-1c265789"><?php echo $udesly_fe_items['text_-1c265789'] ?></h1>
        <h2 class="h2-custom-mug-style" data-udy-fe="text_5279a8d1"><?php echo $udesly_fe_items['text_5279a8d1'] ?></h2>
        <p data-udy-fe="text_4a4a609d"><?php echo $udesly_fe_items['text_4a4a609d'] ?><br></p>
      </div>
    </div>
  </div>
  <div class="products custom-mug-style">
    <div class="page-container _3 w-container">
      <h1 class="h1-custom-mug-style _2" data-udy-fe="text_-6febaea8"><?php echo $udesly_fe_items['text_-6febaea8'] ?></h1>
      <div class="product-wrapper">
        <div id="w-node-50d50a3bd31d-9add9b8f" class="product-desc">
          <h2 class="h2-custom-mug-style" data-udy-fe="text_5081201e"><?php echo $udesly_fe_items['text_5081201e'] ?></h2>
          <p data-udy-fe="text_-55fe43f8,text_200d,text_6d12c1b1,text_-d578b9f,text_d89c458"><?php echo $udesly_fe_items['text_-55fe43f8'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br><?php echo $udesly_fe_items['text_6d12c1b1'] ?><br><?php echo $udesly_fe_items['text_-d578b9f'] ?><br><?php echo $udesly_fe_items['text_d89c458'] ?><br></p>
          <p class="price" data-udy-fe="text_-161f098a"><?php echo $udesly_fe_items['text_-161f098a'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd326-9add9b8f" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-247a68d8']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-247a68d8']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_-247a68d8']->alt; ?>" class="img-product" data-udy-fe="image_-247a68d8"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-24888059']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-24888059']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_-24888059']->alt; ?>" class="img-product" data-udy-fe="image_-24888059"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-50d50a3bd334-9add9b8f" class="product-desc">
          <h1 class="h2-custom-mug-style" data-udy-fe="text_7bdbb639"><?php echo $udesly_fe_items['text_7bdbb639'] ?></h1>
          <p data-udy-fe="text_-55fe43f8,text_200d,text_6d12c1b1,text_-d578b9f,text_d89c458"><?php echo $udesly_fe_items['text_-55fe43f8'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br><?php echo $udesly_fe_items['text_6d12c1b1'] ?><br><?php echo $udesly_fe_items['text_-d578b9f'] ?><br><?php echo $udesly_fe_items['text_d89c458'] ?><br></p>
          <p class="price" data-udy-fe="text_91b1e10"><?php echo $udesly_fe_items['text_91b1e10'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd355-9add9b8f" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-246c5157']->src; ?>" alt="<?php echo $udesly_fe_items['image_-246c5157']->alt; ?>" class="img-product" data-udy-fe="image_-246c5157" srcset="<?php echo $udesly_fe_items['image_-246c5157']->srcset; ?>"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-245e39d6']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-245e39d6']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_-245e39d6']->alt; ?>" class="img-product" data-udy-fe="image_-245e39d6"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-7e24dca5a4a4-9add9b8f" class="product-desc">
          <h2 class="h2-custom-mug-style" data-udy-fe="text_3f793566"><?php echo $udesly_fe_items['text_3f793566'] ?></h2>
          <p data-udy-fe="text_-55fe43f8,text_200d,text_6d12c1b1,text_-d578b9f,text_d89c458"><?php echo $udesly_fe_items['text_-55fe43f8'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br><?php echo $udesly_fe_items['text_6d12c1b1'] ?><br><?php echo $udesly_fe_items['text_-d578b9f'] ?><br><?php echo $udesly_fe_items['text_d89c458'] ?><br></p>
          <p class="price" data-udy-fe="text_5cade914"><?php echo $udesly_fe_items['text_5cade914'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-7e24dca5a4af-9add9b8f" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-24502255']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-24502255']->srcset; ?>" sizes="(max-width: 479px) 88vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_-24502255']->alt; ?>" class="img-product" data-udy-fe="image_-24502255"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-231a1d3f']->src; ?>" alt="<?php echo $udesly_fe_items['image_-231a1d3f']->alt; ?>" class="img-product" data-udy-fe="image_-231a1d3f" srcset="<?php echo $udesly_fe_items['image_-231a1d3f']->srcset; ?>"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-d3c2225ec374-9add9b8f" class="product-desc">
          <h1 class="h2-custom-mug-style" data-udy-fe="text_2c8374d6"><?php echo $udesly_fe_items['text_2c8374d6'] ?></h1>
          <p data-udy-fe="text_-19786d65,text_153b31de,text_-50acd205,text_7e98fda9,text_67e9a8cd"><?php echo $udesly_fe_items['text_-19786d65'] ?><br><?php echo $udesly_fe_items['text_153b31de'] ?><br><?php echo $udesly_fe_items['text_-50acd205'] ?><br><?php echo $udesly_fe_items['text_7e98fda9'] ?><br><?php echo $udesly_fe_items['text_67e9a8cd'] ?><br></p>
          <p class="price" data-udy-fe="text_-6bbbecc"><?php echo $udesly_fe_items['text_-6bbbecc'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-d3c2225ec37f-9add9b8f" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-230c05be']->src; ?>" alt="<?php echo $udesly_fe_items['image_-230c05be']->alt; ?>" class="img-product" data-udy-fe="image_-230c05be" srcset="<?php echo $udesly_fe_items['image_-230c05be']->srcset; ?>"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-22fdee3d']->src; ?>" alt="<?php echo $udesly_fe_items['image_-22fdee3d']->alt; ?>" class="img-product" data-udy-fe="image_-22fdee3d" srcset="<?php echo $udesly_fe_items['image_-22fdee3d']->srcset; ?>"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-bc4c951a619e-9add9b8f" class="product-desc">
          <h2 class="h2-custom-mug-style" data-udy-fe="text_1fef01"><?php echo $udesly_fe_items['text_1fef01'] ?></h2>
          <p data-udy-fe="text_-6392cb76,text_1f19f93f,text_7b853f11,text_7d1cdbd4,text_12b244be,text_68fb16f5,text_5f21053c"><?php echo $udesly_fe_items['text_-6392cb76'] ?><br><?php echo $udesly_fe_items['text_1f19f93f'] ?><br><?php echo $udesly_fe_items['text_7b853f11'] ?><br><?php echo $udesly_fe_items['text_7d1cdbd4'] ?><br><?php echo $udesly_fe_items['text_12b244be'] ?><br><?php echo $udesly_fe_items['text_68fb16f5'] ?><br><?php echo $udesly_fe_items['text_5f21053c'] ?><br></p>
          <p class="price" data-udy-fe="text_-e6d642b"><?php echo $udesly_fe_items['text_-e6d642b'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-bc4c951a61a7-9add9b8f" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-22e1bf3b']->src; ?>" alt="<?php echo $udesly_fe_items['image_-22e1bf3b']->alt; ?>" class="img-product" data-udy-fe="image_-22e1bf3b" srcset="<?php echo $udesly_fe_items['image_-22e1bf3b']->srcset; ?>"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-22efd6bc']->src; ?>" alt="<?php echo $udesly_fe_items['image_-22efd6bc']->alt; ?>" class="img-product" data-udy-fe="image_-22efd6bc" srcset="<?php echo $udesly_fe_items['image_-22efd6bc']->srcset; ?>"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-1d7a3e17f7ba-9add9b8f" class="product-desc">
          <h1 class="h2-custom-mug-style" data-udy-fe="text_22bfc564"><?php echo $udesly_fe_items['text_22bfc564'] ?></h1>
          <p data-udy-fe="text_-d578b9f,text_3e7a229c,text_-5e4db610"><?php echo $udesly_fe_items['text_-d578b9f'] ?><br><?php echo $udesly_fe_items['text_3e7a229c'] ?><br><?php echo $udesly_fe_items['text_-5e4db610'] ?><br></p>
          <p class="price" data-udy-fe="text_76db8ceb"><?php echo $udesly_fe_items['text_76db8ceb'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-1d7a3e17f7c5-9add9b8f" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-22d3a7ba']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-22d3a7ba']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_-22d3a7ba']->alt; ?>" class="img-product" data-udy-fe="image_-22d3a7ba"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-22c59039']->src; ?>" alt="<?php echo $udesly_fe_items['image_-22c59039']->alt; ?>" class="img-product" data-udy-fe="image_-22c59039" srcset="<?php echo $udesly_fe_items['image_-22c59039']->srcset; ?>"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-81814e1ce9f5-9add9b8f" class="product-desc">
          <h2 class="h2-custom-mug-style" data-udy-fe="text_5f2682f6"><?php echo $udesly_fe_items['text_5f2682f6'] ?></h2>
          <p data-udy-fe="text_-d578b9f,text_-3086ab98,text_3cfc2fad,text_306961bc"><?php echo $udesly_fe_items['text_-d578b9f'] ?><br><?php echo $udesly_fe_items['text_-3086ab98'] ?><br><?php echo $udesly_fe_items['text_3cfc2fad'] ?><br><?php echo $udesly_fe_items['text_306961bc'] ?><br></p>
          <p class="price" data-udy-fe="text_76db8ceb"><?php echo $udesly_fe_items['text_76db8ceb'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-81814e1ce9fe-9add9b8f" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-22b778b8']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-22b778b8']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_-22b778b8']->alt; ?>" class="img-product" data-udy-fe="image_-22b778b8"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-22a96137']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-22a96137']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_-22a96137']->alt; ?>" class="img-product" data-udy-fe="image_-22a96137"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="cta-cont w-container">
      <div class="ico-wrapper bigger"><img src="<?php echo $udesly_fe_items['image_-6e0d1373']->src; ?>" width="164" alt="<?php echo $udesly_fe_items['image_-6e0d1373']->alt; ?>" data-udy-fe="image_-6e0d1373" srcset="<?php echo $udesly_fe_items['image_-6e0d1373']->srcset; ?>"></div>
      <div class="text-cont _3">
        <h1 class="h1-custom-mug-style" data-udy-fe="text_2efa875a,text_-2f2c4d9a"><?php echo $udesly_fe_items['text_2efa875a'] ?><br><?php echo $udesly_fe_items['text_-2f2c4d9a'] ?></h1>
        <h2 class="h2-custom-mug-style" data-udy-fe="text_51c16288"><?php echo $udesly_fe_items['text_51c16288'] ?></h2>
        <p data-udy-fe="text_-35be796d,text_5f2a4d10"><?php echo $udesly_fe_items['text_-35be796d'] ?><br><?php echo $udesly_fe_items['text_5f2a4d10'] ?><br></p><a href="<?php echo $udesly_fe_items['link_-58c5d7aa']; ?>" class="custom-mug-style-btn w-button" data-udy-fe="text_-5fa13623,link_-58c5d7aa"><?php echo $udesly_fe_items['text_-5fa13623'] ?></a></div>
    </div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-59cb11f2']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-59cb11f2']->alt; ?>" class="hero-avatar" data-udy-fe="image_-59cb11f2" srcset="<?php echo $udesly_fe_items['image_-59cb11f2']->srcset; ?>">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_483b32a8"><?php echo $udesly_fe_items['text_483b32a8'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_-21de8323"><?php echo $udesly_fe_items['text_-21de8323'] ?></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_798d2a19"><?php echo $udesly_fe_items['text_798d2a19'] ?></h6><a href="<?php echo $udesly_fe_items['link_-592de2d']; ?>" class="hero-social facebook w-inline-block" data-udy-fe="link_-592de2d"></a><a href="<?php echo $udesly_fe_items['link_ba700fc']; ?>" class="hero-social insta w-inline-block" data-udy-fe="link_ba700fc"></a><a href="<?php echo $udesly_fe_items['link_-58c5d7aa']; ?>" class="hero-social whatsapp w-inline-block" data-udy-fe="link_-58c5d7aa"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_6bfea12"><?php echo $udesly_fe_items['text_6bfea12'] ?></h1>
        <p class="p-footer" data-udy-fe="text_6ee53dd6"><?php echo $udesly_fe_items['text_6ee53dd6'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_1b3071a3"><?php echo $udesly_fe_items['text_1b3071a3'] ?></h1>
        <p class="p-footer" data-udy-fe="text_17d02902"><?php echo $udesly_fe_items['text_17d02902'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590665080848" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <style data-udy-fe="text_-7b99e2af"><?php echo $udesly_fe_items['text_-7b99e2af'] ?></style>

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'custom-mug-style'); ?></body></html>