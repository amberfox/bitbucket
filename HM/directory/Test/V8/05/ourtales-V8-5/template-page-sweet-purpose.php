<!DOCTYPE html>
<?php /*
        Template Name: sweet-purpose
        */ ?> 
        <html data-wf-page="5ed0f9604f9b68af7be4dc0a" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  
  
  <meta content="Sweet Purpose" property="twitter:title">
  <meta content="Gift Boxes / Hampers / Surprise Breakfast / Custom Gifts / Custom Balloons" property="twitter:description">
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590762180349" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590762180349" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590762180349" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590762180349" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590762180349" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('sweet-purpose'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="sweet-purpose"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_-5b12c530']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_-5b12c530']->alt; ?>" class="arrow-icon" data-udy-fe="image_-5b12c530" srcset="<?php echo $udesly_fe_items['image_-5b12c530']->srcset; ?>"><div class="our-logo" data-udy-fe="text_6bfa9e79"><?php echo $udesly_fe_items['text_6bfa9e79'] ?></div></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="tales-basic">
    <div data-animation="slide" data-duration="500" data-infinite="1" class="tales-basic-slider w-slider">
      <div class="w-slider-mask">
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_2b79f952']->src; ?>" srcset="<?php echo $udesly_fe_items['image_2b79f952']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_2b79f952']->alt; ?>" class="hero-slide-img" data-udy-fe="image_2b79f952"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper full-width"><img src="<?php echo $udesly_fe_items['image_5c84333c']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5c84333c']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_5c84333c']->alt; ?>" class="hero-slide-img" data-udy-fe="image_5c84333c"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper full-width"><img src="<?php echo $udesly_fe_items['image_5c924abd']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5c924abd']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_5c924abd']->alt; ?>" class="hero-slide-img" data-udy-fe="image_5c924abd"></div>
        </div>
      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav w-round"></div>
    </div>
  </div>
  <div class="intro">
    <div class="page-container w-container">
      <div class="logo-cont"><img src="<?php echo $udesly_fe_items['image_-32451617']->src; ?>" width="200" srcset="<?php echo $udesly_fe_items['image_-32451617']->srcset; ?>" sizes="200px" alt="<?php echo $udesly_fe_items['image_-32451617']->alt; ?>" class="round-logo" data-udy-fe="image_-32451617"></div>
      <div class="text-cont">
        <h1 class="h1-sweet-purpose" data-udy-fe="text_79c21ba0"><?php echo $udesly_fe_items['text_79c21ba0'] ?></h1>
        <h2 class="h2-sweet-purpose" data-udy-fe="text_6d452ad4"><?php echo $udesly_fe_items['text_6d452ad4'] ?></h2>
      </div>
    </div>
  </div>
  <div class="products sweet-purpose">
    <div class="page-container _3 w-container">
      <h1 class="h1-sweet-purpose _2" data-udy-fe="text_79fde845"><?php echo $udesly_fe_items['text_79fde845'] ?></h1>
      <div class="product-wrapper">
        <div id="w-node-50d50a3bd31d-7be4dc0a" class="product-desc">
          <h2 class="h2-sweet-purpose _2" data-udy-fe="text_5918cbe"><?php echo $udesly_fe_items['text_5918cbe'] ?></h2>
          <p data-udy-fe="text_-26c49e6e"><?php echo $udesly_fe_items['text_-26c49e6e'] ?><br></p>
        </div>
        <div id="w-node-50d50a3bd329-7be4dc0a" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5c3dbdb7']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5c3dbdb7']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_5c3dbdb7']->alt; ?>" class="img-product" data-udy-fe="image_5c3dbdb7"></div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-50d50a3bd334-7be4dc0a" class="product-desc">
          <h1 class="h2-sweet-purpose _2" data-udy-fe="text_599271db"><?php echo $udesly_fe_items['text_599271db'] ?></h1>
          <p data-udy-fe="text_-5e0d6383"><?php echo $udesly_fe_items['text_-5e0d6383'] ?><br></p>
        </div>
        <div id="w-node-50d50a3bd358-7be4dc0a" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5c4bd538']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5c4bd538']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_5c4bd538']->alt; ?>" class="img-product" data-udy-fe="image_5c4bd538"></div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-7e24dca5a4a4-7be4dc0a" class="product-desc">
          <h2 class="h2-sweet-purpose _2" data-udy-fe="text_ce5e951"><?php echo $udesly_fe_items['text_ce5e951'] ?></h2>
          <p data-udy-fe="text_21c6345b"><?php echo $udesly_fe_items['text_21c6345b'] ?><br></p>
        </div>
        <div id="w-node-7e24dca5a4b2-7be4dc0a" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5c59ecb9']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5c59ecb9']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_5c59ecb9']->alt; ?>" class="img-product" data-udy-fe="image_5c59ecb9"></div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-d3c2225ec374-7be4dc0a" class="product-desc">
          <h1 class="h2-sweet-purpose _2" data-udy-fe="text_62508777"><?php echo $udesly_fe_items['text_62508777'] ?></h1>
          <p data-udy-fe="text_5e6f0ec8"><?php echo $udesly_fe_items['text_5e6f0ec8'] ?><br></p>
        </div>
        <div id="w-node-d3c2225ec382-7be4dc0a" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5c68043a']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5c68043a']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_5c68043a']->alt; ?>" class="img-product" data-udy-fe="image_5c68043a"></div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-bc4c951a619e-7be4dc0a" class="product-desc">
          <h2 class="h2-sweet-purpose _2" data-udy-fe="text_47801c"><?php echo $udesly_fe_items['text_47801c'] ?></h2>
          <p data-udy-fe="text_1761edc3"><?php echo $udesly_fe_items['text_1761edc3'] ?><br></p>
        </div>
        <div id="w-node-bc4c951a61aa-7be4dc0a" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_5c761bbb']->src; ?>" srcset="<?php echo $udesly_fe_items['image_5c761bbb']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_5c761bbb']->alt; ?>" class="img-product" data-udy-fe="image_5c761bbb"></div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-1d7a3e17f7ba-7be4dc0a" class="product-desc">
          <h1 class="h2-sweet-purpose _2" data-udy-fe="text_-128317a5"><?php echo $udesly_fe_items['text_-128317a5'] ?></h1>
          <p data-udy-fe="text_-71f96de7"><?php echo $udesly_fe_items['text_-71f96de7'] ?><br></p>
        </div>
        <div id="w-node-1d7a3e17f7c8-7be4dc0a" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-635f5852']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-635f5852']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_-635f5852']->alt; ?>" class="img-product" data-udy-fe="image_-635f5852"></div>
      </div>
      <div class="product-wrapper">
        <div id="w-node-81814e1ce9f5-7be4dc0a" class="product-desc">
          <h2 class="h2-sweet-purpose _2" data-udy-fe="text_-2f65db9c"><?php echo $udesly_fe_items['text_-2f65db9c'] ?></h2>
          <p data-udy-fe="text_50f4ebd0"><?php echo $udesly_fe_items['text_50f4ebd0'] ?><br></p>
        </div>
        <div id="w-node-81814e1cea01-7be4dc0a" class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-2e791d11']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-2e791d11']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_-2e791d11']->alt; ?>" class="img-product" data-udy-fe="image_-2e791d11"></div>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="cta-cont w-container">
      <div class="ico-wrapper bigger"><img src="<?php echo $udesly_fe_items['image_-32451617']->src; ?>" width="227" srcset="<?php echo $udesly_fe_items['image_-32451617']->srcset; ?>" sizes="227px" alt="<?php echo $udesly_fe_items['image_-32451617']->alt; ?>" data-udy-fe="image_-32451617"></div>
      <div class="text-cont _3">
        <h1 class="h1-sweet-purpose" data-udy-fe="text_-35d36758"><?php echo $udesly_fe_items['text_-35d36758'] ?></h1>
        <h2 class="h2-sweet-purpose" data-udy-fe="text_ba0af6"><?php echo $udesly_fe_items['text_ba0af6'] ?></h2><a href="<?php echo $udesly_fe_items['link_-6c62243']; ?>" class="sweet-purpose-btn w-button" data-udy-fe="text_-5fa13623,link_-6c62243"><?php echo $udesly_fe_items['text_-5fa13623'] ?></a></div>
    </div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-36624c71']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-36624c71']->alt; ?>" sizes="80px" srcset="<?php echo $udesly_fe_items['image_-36624c71']->srcset; ?>" class="hero-avatar" data-udy-fe="image_-36624c71">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_281dd0d8"><?php echo $udesly_fe_items['text_281dd0d8'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_-2e1293c9"><?php echo $udesly_fe_items['text_-2e1293c9'] ?></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_798d2a19"><?php echo $udesly_fe_items['text_798d2a19'] ?></h6><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social facebook w-inline-block" data-udy-fe="link_23"></a><a href="<?php echo $udesly_fe_items['link_3998a48c']; ?>" class="hero-social insta w-inline-block" data-udy-fe="link_3998a48c"></a><a href="<?php echo $udesly_fe_items['link_-6c62243']; ?>" class="hero-social whatsapp w-inline-block" data-udy-fe="link_-6c62243"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_6bfea12"><?php echo $udesly_fe_items['text_6bfea12'] ?></h1>
        <p class="p-footer" data-udy-fe="text_6ee53dd6"><?php echo $udesly_fe_items['text_6ee53dd6'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_1b3071a3"><?php echo $udesly_fe_items['text_1b3071a3'] ?></h1>
        <p class="p-footer" data-udy-fe="text_17d02902"><?php echo $udesly_fe_items['text_17d02902'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590762180349" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <style data-udy-fe="text_-2b41b03c"><?php echo $udesly_fe_items['text_-2b41b03c'] ?></style>

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'sweet-purpose'); ?></body></html>