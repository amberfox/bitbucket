<!DOCTYPE html>
<?php /*
        Template Name: beauty-lashes-ka
        */ ?> 
        <html data-wf-page="5ecf43994752d34481e5b000" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  
  
  <meta content="Extension Eyelashes" property="twitter:title">
  <meta content="Natural Set / Glam set / Wink set and Refills" property="twitter:description">
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590762180349" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590762180349" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590762180349" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590762180349" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590762180349" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('beauty-lashes-ka'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="beauty-lashes-ka"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_-5b12c530']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_-5b12c530']->alt; ?>" class="arrow-icon" data-udy-fe="image_-5b12c530" srcset="<?php echo $udesly_fe_items['image_-5b12c530']->srcset; ?>"><div class="our-logo" data-udy-fe="text_6bfa9e79"><?php echo $udesly_fe_items['text_6bfa9e79'] ?></div></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="intro">
    <div class="page-container w-container">
      <div class="logo-cont _2"><img src="<?php echo $udesly_fe_items['image_-99575c6']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-99575c6']->srcset; ?>" sizes="270px" alt="<?php echo $udesly_fe_items['image_-99575c6']->alt; ?>" class="round-logo shadow" data-udy-fe="image_-99575c6"></div>
      <div class="text-cont _4">
        <h1 class="h1-beauty-lashes-ka" data-udy-fe="text_5193e911"><?php echo $udesly_fe_items['text_5193e911'] ?></h1>
        <h2 class="h2-beauty-lashes-ka" data-udy-fe="text_77ac12b5"><?php echo $udesly_fe_items['text_77ac12b5'] ?><br></h2>
        <p data-udy-fe="text_522f2acb"><?php echo $udesly_fe_items['text_522f2acb'] ?><br></p>
      </div>
    </div>
  </div>
  <div class="products beauty-lashes-ka">
    <div class="page-container _3 w-container">
      <div class="product-wrapper">
        <h1 id="w-node-31021080f124-81e5b000" class="h1-beauty-lashes-ka _2" data-udy-fe="text_-7ba7a2a7"><?php echo $udesly_fe_items['text_-7ba7a2a7'] ?></h1>
        <div id="w-node-50d50a3bd31d-81e5b000" class="product-desc middle">
          <ul role="list" class="w-list-unstyled">
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="25" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-1248e810"><?php echo $udesly_fe_items['text_-1248e810'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="25" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_22728f25"><?php echo $udesly_fe_items['text_22728f25'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="25" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-430b46c0"><?php echo $udesly_fe_items['text_-430b46c0'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="25" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-7c586e21"><?php echo $udesly_fe_items['text_-7c586e21'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="25" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_365a358f"><?php echo $udesly_fe_items['text_365a358f'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="25" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-3bd0ba2d"><?php echo $udesly_fe_items['text_-3bd0ba2d'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="25" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-720f7ca"><?php echo $udesly_fe_items['text_-720f7ca'] ?><br></p>
            </li>
          </ul>
          <p data-udy-fe="text_6ce6a2d1"><br><?php echo $udesly_fe_items['text_6ce6a2d1'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd326-81e5b000" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_567a5e11']->src; ?>" srcset="<?php echo $udesly_fe_items['image_567a5e11']->srcset; ?>" sizes="(max-width: 479px) 100vw, (max-width: 767px) 86vw, (max-width: 991px) 219.4375px, 283.328125px" alt="<?php echo $udesly_fe_items['image_567a5e11']->alt; ?>" class="img-product" data-udy-fe="image_567a5e11"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_56968d13']->src; ?>" srcset="<?php echo $udesly_fe_items['image_56968d13']->srcset; ?>" sizes="(max-width: 479px) 100vw, (max-width: 767px) 86vw, (max-width: 991px) 219.4375px, 283.328125px" alt="<?php echo $udesly_fe_items['image_56968d13']->alt; ?>" class="img-product" data-udy-fe="image_56968d13"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_56887592']->src; ?>" alt="<?php echo $udesly_fe_items['image_56887592']->alt; ?>" class="img-product" data-udy-fe="image_56887592" srcset="<?php echo $udesly_fe_items['image_56887592']->srcset; ?>"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_56b2bc15']->src; ?>" srcset="<?php echo $udesly_fe_items['image_56b2bc15']->srcset; ?>" sizes="(max-width: 479px) 100vw, (max-width: 767px) 86vw, (max-width: 991px) 219.4375px, 283.328125px" alt="<?php echo $udesly_fe_items['image_56b2bc15']->alt; ?>" class="img-product" data-udy-fe="image_56b2bc15"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="ico-wrapper bigger"><img src="<?php echo $udesly_fe_items['image_-99575c6']->src; ?>" width="247" srcset="<?php echo $udesly_fe_items['image_-99575c6']->srcset; ?>" sizes="(max-width: 479px) 247px, (max-width: 767px) 35vw, 247px" alt="<?php echo $udesly_fe_items['image_-99575c6']->alt; ?>" data-udy-fe="image_-99575c6"></div>
    <div class="text-cont _3">
      <h1 class="h1-beauty-lashes-ka" data-udy-fe="text_-303be1d,text_-7e01782"><?php echo $udesly_fe_items['text_-303be1d'] ?><br><?php echo $udesly_fe_items['text_-7e01782'] ?></h1>
      <h2 class="h2-beauty-lashes-ka" data-udy-fe="text_-6ff83b2,text_-3fe6ab66"><?php echo $udesly_fe_items['text_-6ff83b2'] ?><br><?php echo $udesly_fe_items['text_-3fe6ab66'] ?><br></h2>
      <p data-udy-fe="text_-5e1c38d0,text_4e4a873d"><?php echo $udesly_fe_items['text_-5e1c38d0'] ?><br><?php echo $udesly_fe_items['text_4e4a873d'] ?><br></p><a href="<?php echo $udesly_fe_items['link_-5fbdefaf']; ?>" class="beauty-lashes-ka-btn w-button" data-udy-fe="text_443faa97,link_-5fbdefaf"><?php echo $udesly_fe_items['text_443faa97'] ?></a></div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-7ac5d818']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-7ac5d818']->alt; ?>" sizes="80px" srcset="<?php echo $udesly_fe_items['image_-7ac5d818']->srcset; ?>" class="hero-avatar" data-udy-fe="image_-7ac5d818">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_2ed73173"><?php echo $udesly_fe_items['text_2ed73173'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_2c8f1dd3"><?php echo $udesly_fe_items['text_2c8f1dd3'] ?></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_-71d11282"><?php echo $udesly_fe_items['text_-71d11282'] ?></h6><a href="<?php echo $udesly_fe_items['link_e74c2b2']; ?>" target="_blank" class="hero-social facebook w-inline-block" data-udy-fe="link_e74c2b2"></a><a href="<?php echo $udesly_fe_items['link_-2ae45e9a']; ?>" target="_blank" class="hero-social insta w-inline-block" data-udy-fe="link_-2ae45e9a"></a><a href="<?php echo $udesly_fe_items['link_-5fbdefaf']; ?>" target="_blank" class="hero-social whatsapp w-inline-block" data-udy-fe="link_-5fbdefaf"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_6bfea12"><?php echo $udesly_fe_items['text_6bfea12'] ?></h1>
        <p class="p-footer" data-udy-fe="text_6ee53dd6"><?php echo $udesly_fe_items['text_6ee53dd6'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_1b3071a3"><?php echo $udesly_fe_items['text_1b3071a3'] ?></h1>
        <p class="p-footer" data-udy-fe="text_17d02902"><?php echo $udesly_fe_items['text_17d02902'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590762180349" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <style data-udy-fe="text_20e2f9b2"><?php echo $udesly_fe_items['text_20e2f9b2'] ?></style>

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'beauty-lashes-ka'); ?></body></html>