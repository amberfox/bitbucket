<!DOCTYPE html>
<?php /*
        Template Name: sweet-bakery
        */ ?> 
        <html data-wf-page="5ec49673db25b4405158210f" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  
  
  <meta content="Sweet Bakery" property="twitter:title">
  <meta content="Swiss Roll - Pionono or Brazo de Reina &amp; Alfajor or Cookies with Caramel - Arequipe or Dulce de Leche." property="twitter:description">
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590762180349" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590762180349" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590762180349" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590762180349" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590762180349" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('sweet-bakery'); ?></head>
<body class="<?php echo join(' ', get_body_class() ) . ' body'; ?>" udesly-page="sweet-bakery"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_-5b12c530']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_-5b12c530']->alt; ?>" class="arrow-icon" data-udy-fe="image_-5b12c530" srcset="<?php echo $udesly_fe_items['image_-5b12c530']->srcset; ?>"><div class="our-logo" data-udy-fe="text_6bfa9e79"><?php echo $udesly_fe_items['text_6bfa9e79'] ?></div></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="tales">
    <div class="tale-scroll-track">
      <div class="tale-container">
        <div class="tale-track page">
          <div class="scene-container">
            <div id="w-node-9a66a06ac283-5158210f" class="service-name _1">
              <h1 class="h2-hero" data-udy-fe="text_313ced66,text_651874e"><?php echo $udesly_fe_items['text_313ced66'] ?><br><?php echo $udesly_fe_items['text_651874e'] ?></h1>
              <h1 class="h1-hero" data-udy-fe="text_68c2ef0,text_-533bfe20"><?php echo $udesly_fe_items['text_68c2ef0'] ?><br><?php echo $udesly_fe_items['text_-533bfe20'] ?></h1>
            </div>
          </div>
          <div class="scene-container _2"></div>
          <div class="scene-container _3"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="intro">
    <div class="page-container w-container">
      <div class="logo-cont"><img src="<?php echo $udesly_fe_items['image_29883ba3']->src; ?>" srcset="<?php echo $udesly_fe_items['image_29883ba3']->srcset; ?>" sizes="100vw" alt="<?php echo $udesly_fe_items['image_29883ba3']->alt; ?>" data-udy-fe="image_29883ba3"></div>
      <div class="text-cont">
        <h1 class="h1-sweet-bakery" data-udy-fe="text_-649f705f,text_12d6f22d"><?php echo $udesly_fe_items['text_-649f705f'] ?><br><?php echo $udesly_fe_items['text_12d6f22d'] ?></h1>
        <h2 class="h2-sweet" data-udy-fe="text_-7e3f4622"><?php echo $udesly_fe_items['text_-7e3f4622'] ?></h2>
        <p data-w-id="275b2901-447d-2417-12dc-53847ca06cd3" data-udy-fe="text_47af34fe"><?php echo $udesly_fe_items['text_47af34fe'] ?><br></p>
      </div>
    </div>
  </div>
  <div class="what">
    <div class="page-container _2 w-container">
      <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_-3a4de922']->src; ?>" width="131" srcset="<?php echo $udesly_fe_items['image_-3a4de922']->srcset; ?>" sizes="131px" alt="<?php echo $udesly_fe_items['image_-3a4de922']->alt; ?>" class="ico-sweet-bakery _2" data-udy-fe="image_-3a4de922"></div>
      <div class="text-cont _2">
        <h1 class="h2-sweet" data-udy-fe="text_6dde0d3a"><?php echo $udesly_fe_items['text_6dde0d3a'] ?></h1>
        <p data-udy-fe="text_-5904c839,text_103a7d,text_2ecacf3e,text_-36d9f631,text_-55a77d53"><?php echo $udesly_fe_items['text_-5904c839'] ?><strong data-udy-fe="text_-21c95a28"><?php echo $udesly_fe_items['text_-21c95a28'] ?></strong><?php echo $udesly_fe_items['text_103a7d'] ?><strong data-udy-fe="text_-4f3131bc"><?php echo $udesly_fe_items['text_-4f3131bc'] ?></strong><?php echo $udesly_fe_items['text_2ecacf3e'] ?><strong data-udy-fe="text_-7741696b"><?php echo $udesly_fe_items['text_-7741696b'] ?></strong><?php echo $udesly_fe_items['text_-36d9f631'] ?><br><br><?php echo $udesly_fe_items['text_-55a77d53'] ?><strong data-udy-fe="text_610a1527"><?php echo $udesly_fe_items['text_610a1527'] ?></strong><br></p>
      </div>
    </div>
  </div>
  <div class="what">
    <div class="page-container w-container">
      <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_-3a23a29f']->src; ?>" width="131" alt="<?php echo $udesly_fe_items['image_-3a23a29f']->alt; ?>" class="ico-sweet-bakery" data-udy-fe="image_-3a23a29f" srcset="<?php echo $udesly_fe_items['image_-3a23a29f']->srcset; ?>"></div>
      <div class="text-cont _2">
        <h1 class="h2-sweet" data-udy-fe="text_-14f04171"><?php echo $udesly_fe_items['text_-14f04171'] ?></h1>
        <p data-udy-fe="text_533a12ec,text_6087d1f6,text_-161befb4"><?php echo $udesly_fe_items['text_533a12ec'] ?><strong data-udy-fe="text_-21c95a28"><?php echo $udesly_fe_items['text_-21c95a28'] ?></strong><?php echo $udesly_fe_items['text_6087d1f6'] ?><strong data-udy-fe="text_-36ad1819"><?php echo $udesly_fe_items['text_-36ad1819'] ?></strong><?php echo $udesly_fe_items['text_-161befb4'] ?><br></p>
      </div>
    </div>
  </div>
  <div class="products sweet-bakery">
    <div class="page-container _3 w-container">
      <h1 class="h1-sweet-bakery _2" data-udy-fe="text_-bd8dbce"><?php echo $udesly_fe_items['text_-bd8dbce'] ?></h1>
      <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_-3a3fd1a1']->src; ?>" width="221" srcset="<?php echo $udesly_fe_items['image_-3a3fd1a1']->srcset; ?>" sizes="200px" alt="<?php echo $udesly_fe_items['image_-3a3fd1a1']->alt; ?>" class="ico-sweet-bakery _3" data-udy-fe="image_-3a3fd1a1"></div>
      <div class="product-wrapper">
        <div id="w-node-e3c3d624ce9d-5158210f" class="product-desc">
          <h1 class="h2-sweet" data-udy-fe="text_503b7565"><?php echo $udesly_fe_items['text_503b7565'] ?></h1>
          <p data-udy-fe="text_6d6b8e9b"><?php echo $udesly_fe_items['text_6d6b8e9b'] ?><br></p>
          <p class="price" data-udy-fe="text_4045920c"><?php echo $udesly_fe_items['text_4045920c'] ?><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-a7706e6550c7-5158210f" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-69614d54']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-69614d54']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_-69614d54']->alt; ?>" class="img-product" data-udy-fe="image_-69614d54"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_3a153ad1']->src; ?>" srcset="<?php echo $udesly_fe_items['image_3a153ad1']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 425.328125px, 566.65625px" alt="<?php echo $udesly_fe_items['image_3a153ad1']->alt; ?>" class="img-product" data-udy-fe="image_3a153ad1"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-a8e7363fe674-5158210f" class="product-desc">
          <h1 class="h2-sweet" data-udy-fe="text_-42f9072c"><?php echo $udesly_fe_items['text_-42f9072c'] ?></h1>
          <p data-udy-fe="text_7bf09dac,text_200d,text_66fab2ac,text_66fab2ac"><?php echo $udesly_fe_items['text_7bf09dac'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br><?php echo $udesly_fe_items['text_66fab2ac'] ?><strong data-udy-fe="text_8d45"><?php echo $udesly_fe_items['text_8d45'] ?><br></strong><?php echo $udesly_fe_items['text_66fab2ac'] ?><strong data-udy-fe="text_8d48,text_200d"><?php echo $udesly_fe_items['text_8d48'] ?><br><?php echo $udesly_fe_items['text_200d'] ?></strong><br></p>
          <p data-udy-fe="text_-4f073d34,text_200d,text_66fab2ac,text_66fab2ac"><?php echo $udesly_fe_items['text_-4f073d34'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br><?php echo $udesly_fe_items['text_66fab2ac'] ?><strong data-udy-fe="text_8d48"><?php echo $udesly_fe_items['text_8d48'] ?><br></strong><?php echo $udesly_fe_items['text_66fab2ac'] ?><strong data-udy-fe="text_8d4b,text_200d"><?php echo $udesly_fe_items['text_8d4b'] ?><br><?php echo $udesly_fe_items['text_200d'] ?></strong><br></p>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_-69451e52']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-69451e52']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_-69451e52']->alt; ?>" class="img-product" data-udy-fe="image_-69451e52"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_3a235252']->src; ?>" srcset="<?php echo $udesly_fe_items['image_3a235252']->srcset; ?>" sizes="(max-width: 479px) 79vw, (max-width: 767px) 86vw, (max-width: 991px) 212.65625px, 283.328125px" alt="<?php echo $udesly_fe_items['image_3a235252']->alt; ?>" class="img-product" data-udy-fe="image_3a235252"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="ico-wrapper"><img src="<?php echo $udesly_fe_items['image_-3a31ba20']->src; ?>" width="200" srcset="<?php echo $udesly_fe_items['image_-3a31ba20']->srcset; ?>" sizes="(max-width: 479px) 200px, (max-width: 767px) 31vw, 200px" alt="<?php echo $udesly_fe_items['image_-3a31ba20']->alt; ?>" class="ico-sweet-bakery _4" data-udy-fe="image_-3a31ba20"></div>
    <div class="text-cont _3">
      <h1 class="h1-sweet-bakery _3" data-udy-fe="text_7b8ccf0d,text_-8645c2c"><?php echo $udesly_fe_items['text_7b8ccf0d'] ?><br><?php echo $udesly_fe_items['text_-8645c2c'] ?></h1>
      <h2 class="h2-sweet" data-udy-fe="text_-4383dea4"><?php echo $udesly_fe_items['text_-4383dea4'] ?></h2>
      <p data-udy-fe="text_-6b9b7599,text_2eb14676,text_41768263,text_4909bdb0"><?php echo $udesly_fe_items['text_-6b9b7599'] ?><br><?php echo $udesly_fe_items['text_2eb14676'] ?><br><?php echo $udesly_fe_items['text_41768263'] ?><br><?php echo $udesly_fe_items['text_4909bdb0'] ?><strong></strong><br></p><a href="<?php echo $udesly_fe_items['link_-272b3948']; ?>" class="sweet-bakery-btn w-button" data-udy-fe="text_-5fa13623,link_-272b3948"><?php echo $udesly_fe_items['text_-5fa13623'] ?></a></div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-372c8219']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-372c8219']->alt; ?>" class="hero-avatar" data-udy-fe="image_-372c8219" srcset="<?php echo $udesly_fe_items['image_-372c8219']->srcset; ?>">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_-70ecd892"><?php echo $udesly_fe_items['text_-70ecd892'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_-7e1feb76"><?php echo $udesly_fe_items['text_-7e1feb76'] ?></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_42cccd7d"><?php echo $udesly_fe_items['text_42cccd7d'] ?></h6><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social facebook w-inline-block" data-udy-fe="link_23"></a><a href="<?php echo $udesly_fe_items['link_36e513d9']; ?>" class="hero-social insta w-inline-block" data-udy-fe="link_36e513d9"></a><a href="<?php echo $udesly_fe_items['link_-272b3948']; ?>" class="hero-social whatsapp w-inline-block" data-udy-fe="link_-272b3948"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_6bfea12"><?php echo $udesly_fe_items['text_6bfea12'] ?></h1>
        <p class="p-footer" data-udy-fe="text_6ee53dd6"><?php echo $udesly_fe_items['text_6ee53dd6'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_1b3071a3"><?php echo $udesly_fe_items['text_1b3071a3'] ?></h1>
        <p class="p-footer" data-udy-fe="text_17d02902"><?php echo $udesly_fe_items['text_17d02902'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590762180349" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <style data-udy-fe="text_-438495ac"><?php echo $udesly_fe_items['text_-438495ac'] ?></style>

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'sweet-bakery'); ?></body></html>