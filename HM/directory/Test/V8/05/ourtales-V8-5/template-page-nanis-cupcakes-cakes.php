<!DOCTYPE html>
<?php /*
        Template Name: nanis-cupcakes-cakes
        */ ?> 
        <html data-wf-page="5ecf71729d8fbe218bdfb207" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  
  
  <meta content="Nani's Cupcakes &amp; Cakes" property="twitter:title">
  <meta content="Cake Vanilla / Cake Chocolate / Cake Chocolate and Strawberry / Cake Orange and poppy seed / Genovesa (Tres leches) / Cupcake Vanilla / Cupcake Chocolate" property="twitter:description">
  
  
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590762180349" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590762180349" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590762180349" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Great Vibes:400","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular","Chewy:regular","Permanent Marker:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590762180349" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590762180349" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('nanis-cupcakes-cakes'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="nanis-cupcakes-cakes"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_-5b12c530']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_-5b12c530']->alt; ?>" class="arrow-icon" data-udy-fe="image_-5b12c530" srcset="<?php echo $udesly_fe_items['image_-5b12c530']->srcset; ?>"><div class="our-logo" data-udy-fe="text_6bfa9e79"><?php echo $udesly_fe_items['text_6bfa9e79'] ?></div></a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="tales-basic">
    <div data-animation="slide" data-duration="500" data-infinite="1" class="tales-basic-slider w-slider">
      <div class="w-slider-mask">
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_7aba1a14']->src; ?>" srcset="<?php echo $udesly_fe_items['image_7aba1a14']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_7aba1a14']->alt; ?>" class="hero-slide-img" data-udy-fe="image_7aba1a14"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper"><img src="<?php echo $udesly_fe_items['image_7e23cb52']->src; ?>" srcset="<?php echo $udesly_fe_items['image_7e23cb52']->srcset; ?>" sizes="(max-width: 991px) 100vw, 940px" alt="<?php echo $udesly_fe_items['image_7e23cb52']->alt; ?>" class="hero-slide-img" data-udy-fe="image_7e23cb52"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="slide-img-wrapper buttom"><img src="<?php echo $udesly_fe_items['image_775068d6']->src; ?>" alt="<?php echo $udesly_fe_items['image_775068d6']->alt; ?>" class="hero-slide-img" data-udy-fe="image_775068d6" srcset="<?php echo $udesly_fe_items['image_775068d6']->srcset; ?>"></div>
        </div>
      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav w-round"></div>
    </div>
  </div>
  <div class="intro">
    <div class="page-container w-container">
      <div class="text-cont">
        <h1 class="h1-dianitamado" data-udy-fe="text_112d9c44"><?php echo $udesly_fe_items['text_112d9c44'] ?></h1>
        <h2 class="h2-dianitamado" data-udy-fe="text_-58621d17"><?php echo $udesly_fe_items['text_-58621d17'] ?></h2>
      </div>
    </div>
  </div>
  <div class="products dianitamado">
    <div class="page-container _3 w-container">
      <h1 class="h1-dianitamado _2" data-udy-fe="text_-6febaea8"><?php echo $udesly_fe_items['text_-6febaea8'] ?></h1>
      <div class="product-wrapper">
        <div id="w-node-50d50a3bd31d-8bdfb207" class="product-desc">
          <h1 class="h2-dianitamado _2" data-udy-fe="text_3dddd1b"><?php echo $udesly_fe_items['text_3dddd1b'] ?></h1>
          <ul role="list" class="w-list-unstyled">
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="15" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_711d735b"><?php echo $udesly_fe_items['text_711d735b'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="15" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-5f8a7de4"><?php echo $udesly_fe_items['text_-5f8a7de4'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="15" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-ebd5624"><?php echo $udesly_fe_items['text_-ebd5624'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="15" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-2c97af0e"><?php echo $udesly_fe_items['text_-2c97af0e'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="15" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_1a16688d"><?php echo $udesly_fe_items['text_1a16688d'] ?><br></p>
            </li>
          </ul>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd326-8bdfb207" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_3f57cb5']->src; ?>" alt="<?php echo $udesly_fe_items['image_3f57cb5']->alt; ?>" class="img-product" data-udy-fe="image_3f57cb5" srcset="<?php echo $udesly_fe_items['image_3f57cb5']->srcset; ?>"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_4039436']->src; ?>" alt="<?php echo $udesly_fe_items['image_4039436']->alt; ?>" class="img-product" data-udy-fe="image_4039436" srcset="<?php echo $udesly_fe_items['image_4039436']->srcset; ?>"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
      <div class="product-wrapper _2">
        <div id="w-node-50d50a3bd334-8bdfb207" class="product-desc">
          <h1 class="h2-dianitamado _2" data-udy-fe="text_21e765dd"><?php echo $udesly_fe_items['text_21e765dd'] ?></h1>
          <ul role="list" class="w-list-unstyled">
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="15" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_711d735b"><?php echo $udesly_fe_items['text_711d735b'] ?><br></p>
            </li>
            <li class="list-bullet-check"><img src="<?php echo $udesly_fe_items['image_4e5052a7']->src; ?>" width="15" alt="<?php echo $udesly_fe_items['image_4e5052a7']->alt; ?>" data-udy-fe="image_4e5052a7" srcset="<?php echo $udesly_fe_items['image_4e5052a7']->srcset; ?>">
              <p class="p-list-bullet" data-udy-fe="text_-5f8a7de4"><?php echo $udesly_fe_items['text_-5f8a7de4'] ?><br></p>
            </li>
          </ul>
        </div>
        <div data-animation="slide" data-duration="500" data-infinite="1" id="w-node-50d50a3bd355-8bdfb207" class="slider-product w-slider">
          <div class="w-slider-mask">
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_3cb3632']->src; ?>" alt="<?php echo $udesly_fe_items['image_3cb3632']->alt; ?>" class="img-product" data-udy-fe="image_3cb3632" srcset="<?php echo $udesly_fe_items['image_3cb3632']->srcset; ?>"></div>
            </div>
            <div class="w-slide">
              <div class="img-pr-wrapper"><img src="<?php echo $udesly_fe_items['image_411abb7']->src; ?>" alt="<?php echo $udesly_fe_items['image_411abb7']->alt; ?>" class="img-product" data-udy-fe="image_411abb7" srcset="<?php echo $udesly_fe_items['image_411abb7']->srcset; ?>"></div>
            </div>
          </div>
          <div class="w-slider-arrow-left">
            <div class="w-icon-slider-left"></div>
          </div>
          <div class="w-slider-arrow-right">
            <div class="w-icon-slider-right"></div>
          </div>
          <div class="slide-nav w-slider-nav w-round"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="cta">
    <div class="ico-wrapper bigger"><img src="<?php echo $udesly_fe_items['image_72cc8a66']->src; ?>" alt="<?php echo $udesly_fe_items['image_72cc8a66']->alt; ?>" class="ico-round-shadow" data-udy-fe="image_72cc8a66" srcset="<?php echo $udesly_fe_items['image_72cc8a66']->srcset; ?>"></div>
    <div class="text-cont _3">
      <h1 class="h1-dianitamado _3" data-udy-fe="text_2efa875a,text_47a634b7"><?php echo $udesly_fe_items['text_2efa875a'] ?><br><?php echo $udesly_fe_items['text_47a634b7'] ?></h1>
      <h2 class="h2-dianitamado" data-udy-fe="text_-8755b50"><?php echo $udesly_fe_items['text_-8755b50'] ?></h2>
      <p data-udy-fe="text_76c34c19"><?php echo $udesly_fe_items['text_76c34c19'] ?><br></p><a href="<?php echo $udesly_fe_items['link_-63f2e9ab']; ?>" class="dianitamado-btn w-button" data-udy-fe="text_-5fa13623,link_-63f2e9ab"><?php echo $udesly_fe_items['text_-5fa13623'] ?></a></div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-34ba4962d308-4962d306" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-fcff877']->src; ?>" id="w-node-34ba4962d309-4962d306" alt="<?php echo $udesly_fe_items['image_-fcff877']->alt; ?>" sizes="80px" srcset="<?php echo $udesly_fe_items['image_-fcff877']->srcset; ?>" class="hero-avatar" data-udy-fe="image_-fcff877">
        <h3 id="w-node-34ba4962d30a-4962d306" class="store-owner sweet-bakery" data-udy-fe="text_c994584"><?php echo $udesly_fe_items['text_c994584'] ?></h3>
        <p id="w-node-34ba4962d30c-4962d306" class="p-owner" data-udy-fe="text_40b5295a"><?php echo $udesly_fe_items['text_40b5295a'] ?><br></p>
        <div id="w-node-34ba4962d310-4962d306" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_2fc6beb0"><?php echo $udesly_fe_items['text_2fc6beb0'] ?></h6><a href="<?php echo $udesly_fe_items['link_-1017652d']; ?>" class="hero-social facebook w-inline-block" data-udy-fe="link_-1017652d"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social insta w-inline-block" data-udy-fe="link_23"></a><a href="<?php echo $udesly_fe_items['link_-63f2e9ab']; ?>" class="hero-social whatsapp w-inline-block" data-udy-fe="link_-63f2e9ab"></a><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="hero-social linkedin w-inline-block" data-udy-fe="link_23"></a></div>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_6bfea12"><?php echo $udesly_fe_items['text_6bfea12'] ?></h1>
        <p class="p-footer" data-udy-fe="text_6ee53dd6"><?php echo $udesly_fe_items['text_6ee53dd6'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_1b3071a3"><?php echo $udesly_fe_items['text_1b3071a3'] ?></h1>
        <p class="p-footer" data-udy-fe="text_17d02902"><?php echo $udesly_fe_items['text_17d02902'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590762180349" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <style data-udy-fe="text_-3c7ac09a"><?php echo $udesly_fe_items['text_-3c7ac09a'] ?></style>

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'nanis-cupcakes-cakes'); ?></body></html>