<!DOCTYPE html>
<html data-wf-page="5ec660ef7339f53c7c418452" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  <meta content="Home Backup" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590630606523" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590630606523" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590630606523" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590630606523" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590630606523" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('index'); ?></head>
<body class="<?php echo join(' ', get_body_class() ) . ' body'; ?>" udesly-page="index"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar w-nav">
    <div class="navbar-cont w-container">
      <a href="<?php echo $udesly_fe_items['link_23']; ?>" class="brand w-nav-brand" data-udy-fe="link_23">
        <div class="our-logo" data-udy-fe="text_4666edf9"><?php echo $udesly_fe_items['text_4666edf9'] ?></div>
      </a>
      <div class="city" data-udy-fe="text_-6ce4e26b"><?php echo $udesly_fe_items['text_-6ce4e26b'] ?></div>
    </div>
  </div>
  <div class="welcome">
    <div class="w-container">
      <div class="center-mini-section">
        <h1 class="heading" data-udy-fe="text_609b7574"><?php echo $udesly_fe_items['text_609b7574'] ?></h1>
        <p data-udy-fe="text_-2af2a23a"><?php echo $udesly_fe_items['text_-2af2a23a'] ?></p>
      </div>
    </div>
  </div>
  <div class="tales stores">
    <h2 class="heading h2-home" data-udy-fe="text_-533bfe20"><?php echo $udesly_fe_items['text_-533bfe20'] ?></h2>
    <div class="tale-scroll-track">
      <div class="tale-container w-dyn-list">
        <?php $the_query = udesly_get_content_query('home-bakery'); ?><?php if ( $the_query->have_posts() ) : ?><div class="tale-track w-dyn-items" data-max-pages="<?php echo $the_query->max_num_pages ?>">
          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?><div class="store-container w-dyn-item"><a href="<?php the_permalink(); ?>" class="store-thumb w-inline-block"><img src="<?php the_post_thumbnail_url('full') ?>" alt="<?php echo get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ); ?>" class="img-store"></a>
            <div class="store-cat-tag-wrapper">
              <ul role="list" class="store-category-list">
                <li><?php $main_cat = udesly_get_main_category(get_the_ID()); if($main_cat) : ?><a href="<?php echo $main_cat->url; ?>" class="store-category"><?php echo $main_cat->name; ?></a><?php endif; ?></li>
              </ul>
              <?php foreach( udesly_blog_get_categories(2) as $category) : ?><ul role="list" class="store-category-list" href="<?php echo $category->link; ?>"><?php echo $category->name; ?></ul><?php endforeach; ?>
            </div>
            <h2 class="h2-store-name"><?php the_title(); ?></h2>
            <p class="p-store-excerpt"><?php echo get_the_excerpt(); ?></p>
          </div><?php endwhile; wp_reset_query(); ?>
        </div>
        <?php else : ?><div class="w-dyn-empty">
          <div data-udy-fe="text_3d503d2b"><?php echo $udesly_fe_items['text_3d503d2b'] ?></div>
        </div><?php endif; ?>
      </div>
    </div>
  </div>
  <div class="tales stores">
    <h2 class="heading h2-home" data-udy-fe="text_3cffaa7b"><?php echo $udesly_fe_items['text_3cffaa7b'] ?></h2>
    <div class="tale-scroll-track">
      <div class="tale-container w-dyn-list">
        <?php $the_query = udesly_get_content_query('home-food'); ?><?php if ( $the_query->have_posts() ) : ?><div class="tale-track w-dyn-items" data-max-pages="<?php echo $the_query->max_num_pages ?>">
          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?><div class="store-container w-dyn-item"><a href="<?php the_permalink(); ?>" class="store-thumb w-inline-block"><img src="<?php the_post_thumbnail_url('full') ?>" alt="<?php echo get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ); ?>" class="img-store"></a>
            <div class="store-cat-tag-wrapper">
              <ul role="list" class="store-category-list">
                <li><?php $main_cat = udesly_get_main_category(get_the_ID()); if($main_cat) : ?><a href="<?php echo $main_cat->url; ?>" class="store-category"><?php echo $main_cat->name; ?></a><?php endif; ?></li>
              </ul>
              <?php foreach( udesly_blog_get_categories(2) as $category) : ?><ul role="list" class="store-category-list" href="<?php echo $category->link; ?>"><?php echo $category->name; ?></ul><?php endforeach; ?>
            </div>
            <h2 class="h2-store-name"><?php the_title(); ?></h2>
            <p class="p-store-excerpt"><?php echo get_the_excerpt(); ?></p>
          </div><?php endwhile; wp_reset_query(); ?>
        </div>
        <?php else : ?><div class="w-dyn-empty">
          <div data-udy-fe="text_3d503d2b"><?php echo $udesly_fe_items['text_3d503d2b'] ?></div>
        </div><?php endif; ?>
      </div>
    </div>
  </div>
  <div class="tales stores">
    <h2 class="heading h2-home" data-udy-fe="text_765cfc9c"><?php echo $udesly_fe_items['text_765cfc9c'] ?></h2>
    <div class="tale-scroll-track">
      <div class="tale-container w-dyn-list">
        <?php $the_query = udesly_get_content_query('home-beauty'); ?><?php if ( $the_query->have_posts() ) : ?><div class="tale-track w-dyn-items" data-max-pages="<?php echo $the_query->max_num_pages ?>">
          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?><div class="store-container w-dyn-item"><a href="<?php the_permalink(); ?>" class="store-thumb w-inline-block"><img src="<?php the_post_thumbnail_url('full') ?>" alt="<?php echo get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ); ?>" class="img-store"></a>
            <div class="store-cat-tag-wrapper">
              <ul role="list" class="store-category-list">
                <li><?php $main_cat = udesly_get_main_category(get_the_ID()); if($main_cat) : ?><a href="<?php echo $main_cat->url; ?>" class="store-category"><?php echo $main_cat->name; ?></a><?php endif; ?></li>
              </ul>
              <?php foreach( udesly_blog_get_categories(2) as $category) : ?><ul role="list" class="store-category-list" href="<?php echo $category->link; ?>"><?php echo $category->name; ?></ul><?php endforeach; ?>
            </div>
            <h2 class="h2-store-name"><?php the_title(); ?></h2>
            <p class="p-store-excerpt"><?php echo get_the_excerpt(); ?></p>
          </div><?php endwhile; wp_reset_query(); ?>
        </div>
        <?php else : ?><div class="w-dyn-empty">
          <div data-udy-fe="text_3d503d2b"><?php echo $udesly_fe_items['text_3d503d2b'] ?></div>
        </div><?php endif; ?>
      </div>
    </div>
  </div>
  <div id="Footer" class="footer">
    <div class="footer-cont w-container">
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_6bfea12"><?php echo $udesly_fe_items['text_6bfea12'] ?></h1>
        <p class="p-footer" data-udy-fe="text_6ee53dd6"><?php echo $udesly_fe_items['text_6ee53dd6'] ?></p>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_-2aa852a5"><?php echo $udesly_fe_items['text_-2aa852a5'] ?></h1>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('footer'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('social-links-menu'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
      <div class="footer-col-wrapper">
        <h1 class="heading h3-footer" data-udy-fe="text_1b3071a3"><?php echo $udesly_fe_items['text_1b3071a3'] ?></h1>
        <p class="p-footer" data-udy-fe="text_17d02902"><?php echo $udesly_fe_items['text_17d02902'] ?></p>
        <ul role="list" class="w-list-unstyled">
          <?php $c_menu = wp_get_nav_menu_items('registration'); if ($c_menu) : foreach( $c_menu as $link ) : ?><li><a href="<?php echo $link->url; ?>" target="_blank" class="footer-a"><?php echo $link->title; ?></a></li><?php endforeach; endif; ?>
        </ul>
      </div>
    </div>
    <div class="p-footer copy" data-udy-fe="text_7b25f8c9"><?php echo $udesly_fe_items['text_7b25f8c9'] ?></div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590630606523" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'index'); ?></body></html>