<!DOCTYPE html>
<html data-wf-page="5ecc8e9bf9b509646df2e3b4" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  <meta content="Search Results" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590630606523" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590630606523" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590630606523" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Roboto:100,300,regular,700","Rancho:regular","Black Han Sans:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590630606523" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590630606523" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
@media screen and (min-width: 991px) {
/* width */
.tale-scroll-track ::-webkit-scrollbar {
  width: 5px;
  display: inline;
}
/* Track */
.tale-scroll-track ::-webkit-scrollbar-track {
  background-color: transparent !important;
}
/* Handle */
.tale-scroll-track ::-webkit-scrollbar-thumb {
  background: #f0f8ff;
  border-radius: 5px;
  transition: all 0.5s ease;
  width: 20px;
}
/* Handle on hover */
.tale-scroll-track ::-webkit-scrollbar-thumb:hover {
  background: #defffc; 
  transition: all 0.5s ease;
}
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('search'); ?></head>
<body class="<?php echo join(' ', get_body_class() ); ?>" udesly-page="search">
  <div class="w-container">
    <h1 data-udy-fe="text_7e85e19e"><?php echo $udesly_fe_items['text_7e85e19e'] ?></h1>
    <form action="<?php echo home_url(''); ?>" class="w-form" method="GET"><input type="search" class="search-input w-input" maxlength="256" name="s" placeholder="Search…" id="search" required=""><input type="submit" value="Search" class="search-button w-button"><input type="hidden" value="" name="post_type" id="post_type"></form>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590630606523" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php udesly_set_fe_configuration($udesly_fe_items, 'search'); ?></body></html>