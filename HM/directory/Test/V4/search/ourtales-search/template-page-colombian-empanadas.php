<!DOCTYPE html>
<?php /*
        Template Name: colombian-empanadas
        */ ?> 
        <html data-wf-page="5ec622ee034337779c0e5c7d" data-wf-site="5ec12dbcacbc56b3fb0a9021"><head>
  <meta charset="utf-8">
  
  
  <meta content="Colombian Empanadas" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/normalize.css?v=1590471127613" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/webflow.css?v=1590471127613" rel="stylesheet" type="text/css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/ourtales.webflow.css?v=1590471127613" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Changa One:400,400italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Roboto:100,300,regular,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1590471127613" rel="shortcut icon" type="image/x-icon">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/images/webclip.png?v=1590471127613" rel="apple-touch-icon">
  <style>
.tale-scroll-track ::-webkit-scrollbar {
  display: none;
}
.tale-container {
  overflow-y: hidden; /* Hide vertical scrollbar */
}
</style>
<?php wp_enqueue_script("jquery"); wp_head(); ?><?php $udesly_fe_items = udesly_set_fe_items('colombian-empanadas'); ?></head>
<body class="<?php echo join(' ', get_body_class() ) . ' body'; ?>" udesly-page="colombian-empanadas"><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar detail w-nav">
    <div class="container w-container"><a href="<?php echo $udesly_fe_items['link_29d0cb47']; ?>" class="brand w-nav-brand" data-udy-fe="link_29d0cb47"><img src="<?php echo $udesly_fe_items['image_-5b12c530']->src; ?>" width="40" height="40" alt="<?php echo $udesly_fe_items['image_-5b12c530']->alt; ?>" class="arrow-icon" data-udy-fe="image_-5b12c530" srcset="<?php echo $udesly_fe_items['image_-5b12c530']->srcset; ?>"><div class="our-logo" data-udy-fe="text_33ce569f"><?php echo $udesly_fe_items['text_33ce569f'] ?></div></a></div>
  </div>
  <div class="tales-basic">
    <div data-animation="slide" data-duration="500" data-infinite="1" class="tales-basic-slider w-slider">
      <div class="w-slider-mask">
        <div class="tales-basic-slide w-slide">
          <div class="basic-scene-cont"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="basic-scene-cont _2"></div>
        </div>
        <div class="tales-basic-slide w-slide">
          <div class="basic-scene-cont _3"></div>
        </div>
      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav w-round"></div>
    </div>
  </div>
  <div>
    <div class="container-2 w-container">
      <div class="text-cont basic">
        <h1 class="heading basic" data-udy-fe="text_-1e9d13c4"><?php echo $udesly_fe_items['text_-1e9d13c4'] ?></h1>
        <p data-udy-fe="text_-32087c29,text_-2fb1bf05"><?php echo $udesly_fe_items['text_-32087c29'] ?><br><br><?php echo $udesly_fe_items['text_-2fb1bf05'] ?><br></p>
        <h2 class="heading basic h2" data-udy-fe="text_-293b7bc9"><?php echo $udesly_fe_items['text_-293b7bc9'] ?></h2>
        <p data-udy-fe="text_-33544976,text_-5cf92156,text_200d,text_69241343,text_-17af7f1f,text_200d,text_-54b668c3,text_-2893a4b3"><?php echo $udesly_fe_items['text_-33544976'] ?><strong data-udy-fe="text_40e806ef"><?php echo $udesly_fe_items['text_40e806ef'] ?></strong><?php echo $udesly_fe_items['text_-5cf92156'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br><strong data-udy-fe="text_54b14508"><?php echo $udesly_fe_items['text_54b14508'] ?></strong><br><br><?php echo $udesly_fe_items['text_69241343'] ?><br><?php echo $udesly_fe_items['text_-17af7f1f'] ?><br><?php echo $udesly_fe_items['text_200d'] ?><br><strong data-udy-fe="text_9307c9a,text_200d"><?php echo $udesly_fe_items['text_9307c9a'] ?><br><?php echo $udesly_fe_items['text_200d'] ?></strong><br><?php echo $udesly_fe_items['text_-54b668c3'] ?><br><br><?php echo $udesly_fe_items['text_-2893a4b3'] ?><br></p>
        <h2 class="heading basic h2" data-udy-fe="text_-2a48f137"><?php echo $udesly_fe_items['text_-2a48f137'] ?></h2>
        <p data-udy-fe="text_-477a5625,text_2d18b524,text_302fd6c9,text_1559da71"><?php echo $udesly_fe_items['text_-477a5625'] ?><strong data-udy-fe="text_-60ce40b3"><?php echo $udesly_fe_items['text_-60ce40b3'] ?></strong><?php echo $udesly_fe_items['text_2d18b524'] ?><br><br><strong data-udy-fe="text_34ef8014"><?php echo $udesly_fe_items['text_34ef8014'] ?></strong><?php echo $udesly_fe_items['text_302fd6c9'] ?><br><br><strong data-udy-fe="text_40c39afa"><?php echo $udesly_fe_items['text_40c39afa'] ?></strong><?php echo $udesly_fe_items['text_1559da71'] ?><br></p>
      </div><a href="<?php echo $udesly_fe_items['link_23']; ?>" class="basic-btn w-button" data-udy-fe="text_-5fa13623,link_23"><?php echo $udesly_fe_items['text_-5fa13623'] ?></a></div>
  </div>
  <div class="owner">
    <div class="container-owner w-container">
      <div id="w-node-4a48025ef241-9c0e5c7d" class="hero-store"><img src="<?php echo $udesly_fe_items['image_-26767a3d']->src; ?>" srcset="<?php echo $udesly_fe_items['image_-26767a3d']->srcset; ?>" sizes="80px" id="w-node-4a48025ef242-9c0e5c7d" alt="<?php echo $udesly_fe_items['image_-26767a3d']->alt; ?>" class="hero-avatar col-empanadas" data-udy-fe="image_-26767a3d">
        <h3 id="w-node-4a48025ef243-9c0e5c7d" class="store-owner col-empanada" data-udy-fe="text_5456d081"><?php echo $udesly_fe_items['text_5456d081'] ?></h3>
        <p id="w-node-4a48025ef245-9c0e5c7d" class="p-owner" data-udy-fe="text_3f1ae3e3"><?php echo $udesly_fe_items['text_3f1ae3e3'] ?><br></p>
        <div id="w-node-4a48025ef249-9c0e5c7d" class="hero-social-wrapper">
          <h6 class="h6-owner" data-udy-fe="text_42cccd7d"><?php echo $udesly_fe_items['text_42cccd7d'] ?></h6><img src="<?php echo $udesly_fe_items['image_-2712ac8']->src; ?>" alt="<?php echo $udesly_fe_items['image_-2712ac8']->alt; ?>" class="hero-social" data-udy-fe="image_-2712ac8" srcset="<?php echo $udesly_fe_items['image_-2712ac8']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_19eea9bf']->src; ?>" alt="<?php echo $udesly_fe_items['image_19eea9bf']->alt; ?>" class="hero-social" data-udy-fe="image_19eea9bf" srcset="<?php echo $udesly_fe_items['image_19eea9bf']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_-4cb31090']->src; ?>" alt="<?php echo $udesly_fe_items['image_-4cb31090']->alt; ?>" class="hero-social" data-udy-fe="image_-4cb31090" srcset="<?php echo $udesly_fe_items['image_-4cb31090']->srcset; ?>"><img src="<?php echo $udesly_fe_items['image_5d635e04']->src; ?>" alt="<?php echo $udesly_fe_items['image_5d635e04']->alt; ?>" class="hero-social" data-udy-fe="image_5d635e04" srcset="<?php echo $udesly_fe_items['image_5d635e04']->srcset; ?>"></div>
      </div>
    </div>
  </div>
  
  <script type="text/javascript">var $ = window.jQuery;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/js/webflow.js?v=1590471127613" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

<?php wp_footer(); ?><?php endwhile; endif; ?><?php udesly_set_fe_configuration($udesly_fe_items, 'colombian-empanadas'); ?></body></html>